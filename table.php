<!-- Tablesorter: required for bootstrap -->
<link rel="stylesheet" href="assets/theme.bootstrap.css">
<script src="vendors/jquery.tablesorter.js"></script>
<script src="vendors/jquery.tablesorter.widgets.js"></script>

<!-- Tablesorter: optional -->
<link rel="stylesheet" href="assets/jquery.tablesorter.pager.css">

<script src="assets/jquery.tablesorter.pager.js"></script>

<script id="js">$(function() {

$.extend($.tablesorter.themes.bootstrap, {
	table      : 'table table-bordered',
	caption    : 'caption',
	header     : 'bootstrap-header', // give the header a gradient background
	footerRow  : '',
	footerCells: '',
	icons      : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
	sortNone   : 'bootstrap-icon-unsorted',
	sortAsc    : 'icon-chevron-up glyphicon glyphicon-chevron-up',     // includes classes for Bootstrap v2 & v3
	sortDesc   : 'icon-chevron-down glyphicon glyphicon-chevron-down', // includes classes for Bootstrap v2 & v3
	active     : '', // applied when column is sorted
	hover      : '', // use custom css here - bootstrap class may not override it
	filterRow  : '', // filter row class
	even       : '', // odd row zebra striping
	odd        : ''  // even row zebra striping
});

$("table").tablesorter({
	theme : "bootstrap",
	widthFixed: true,
	headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!
	widgets : [ "uitheme", "filter", "zebra" ],

	widgetOptions : {
		zebra : ["even", "odd"],

		filter_reset : ".reset"
	}
})
.tablesorterPager({

	container: $(".ts-pager"),
	cssGoto  : ".pagenum",
	removeRows: false,
	output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

});

});</script>

<script>
$(function(){

	$('button.filter').click(function(){
		var col = $(this).data('column'),
			txt = $(this).data('filter');
		$('table').find('.tablesorter-filter').val('').eq(col).val(txt);
		$('table').trigger('search', false);
		return false;
	});

	$('button.zebra').click(function(){
		var t = $(this).hasClass('btn-success');
		$('table')
			.toggleClass('table-striped')[0]
			.config.widgets = (t) ? ["uitheme", "filter"] : ["uitheme", "filter", "zebra"];
		$(this)
			.toggleClass('btn-danger btn-success')
			.find('i')
			.toggleClass('icon-ok icon-remove glyphicon-ok glyphicon-remove').end()
			.find('span')
			.text(t ? 'disabled' : 'enabled');
		$('table').trigger('refreshWidgets', [false]);
		return false;
	});
});
</script>