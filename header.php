<?php 
    require("seguranca.php"); // Inclui o arquivo com o sistema de seguran�a
    protegePagina(); // Chama a fun��o que protege a p�gina
?>
<!DOCTYPE html>
<html class="no-vendors">
    <head>
        <title>MPS // Monitoramento Preventivo e Sistemas</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link href="assets/modal.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.vendors"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.vendors"></script>

    </head>
    
     <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <img src="images/logo-top.png" class="brand"><a class="brand" href="index.php">MPS</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
							<li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo" "; echo $_SESSION['usuarioNome']?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="0" id="create-user">Alterar Senha</a>
                                    </li>	
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>								
                                </ul>
                            </li>
							
                        </ul>
							<ul class="nav pull-right">
								<li><a href="#"><i class="icon-tag"></i><?php echo" "; echo $_SESSION['usuarioLogin']?></a></li>	
							</ul>   
                        <ul class="nav">
                            <li class="active">
                                <a href="./">Dashboard</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Alagamentos <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" href="alagamentos.php#mapa-alagamentos">Mapa</a></li>
                                    <li><a tabindex="-1" href="alagamentos.php#form-alagamentos">Cadastrar novo Ponto</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Alertas <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" href="alertas.php#lista-alertas">Listar</a></li>
                                    <li><a tabindex="-1" href="alertas.php#form-alertas">Cadastrar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Clima <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" href="clima.php#form-clima">Inserir Informa��es</a></li>
                                    <li><a tabindex="-1" href="clima.php#graficos-clima">Gr�ficos</a></li>
                                </ul>
                            </li>
                            <li><a href="sumario_incidentes.php">Incidentes</a></li>							
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
						<li><a class="ajax-link" href="index.php"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
						<li><a class="ajax-link" href="clima.php"><i class="icon-leaf"></i><span class="hidden-tablet"> Clima</span></a></li>
						<li><a class="ajax-link" href="alagamentos.php"><i class="icon-leaf"></i><span class="hidden-tablet"> Pontos de Alagamento</span></a></li>
						<li><a class="ajax-link" href="alertas.php"><i class="icon-exclamation-sign"></i><span class="hidden-tablet"> Alertas</span></a></li>
						<li><a class="ajax-link" href="copa.php"><i class="icon-map-marker"></i><span class="hidden-tablet"> Copa</span></a></li>
						<li><a class="ajax-link" href="acoes_greve.php"><i class="icon-map-marker"></i><span class="hidden-tablet"> A��es para Greve</span></a></li>
						<li><a class="ajax-link" href="usuarios.php"><i class="icon-user"></i><span class="hidden-tablet"> Usu&aacuterios</span></a></li>
						<li><a class="ajax-link" href="sumario_incidentes.php"><i class="icon-list-alt"></i><span class="hidden-tablet"> Sum�rio de Incidentes</span></a></li>
						<li><a class="ajax-link" href="2_e_3.php"><i class="icon-list-alt"></i><span class="hidden-tablet"> Incidentes 2 e 3</span></a></li>
						<li><a class="ajax-link" href="boletim.php"><i class="icon-web"></i><span class="hidden-tablet"> Boletim Informativo</span></a></li>
						<li><a class="ajax-link" href="plantao.php"><i class="icon-web"></i><span class="hidden-tablet"> Plant�o Torre</span></a></li>
                    </ul>
                </div>