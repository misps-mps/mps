<?php
    
    require('src/db.php');

    $db = new Database();


    //Transferir o arquivo
    if (isset($_POST['submit'])) {
      
        if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
            echo "<h1>" . "O arquivo ". $_FILES['filename']['name'] ." foi transferido com sucesso." . "</h1>";
        }
      
        //Importar o arquivo transferido para o banco de dados
        $handle = fopen($_FILES['filename']['tmp_name'], "r");

        #
        if(!$handle){
            echo "Não leu o arquivo: ".$_FILES['filename']['tmp_name'];
            exit;
        }
        
        #
        while(($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

            # id unica do incidente
            $uid = hash('md5', time().$data[3]);

            $status = "SELECT id FROM mps.inc_status WHERE nome='$data[2]'";
            $categ = ($data[7] == '') ? 0 : "SELECT id FROM mps.inc_categorias WHERE nome='$data[7]'" ;

            # dados do incidente
            $import1 = "
            INSERT INTO mps.inc_incidentes (uid, status, numero, severidade, categoria, titulo, descricao, inicio, fim, criacao)
            VALUES ('$uid', ($status), '$data[3]', '$data[4]', '$categ', '$data[5]', '$data[6]', '$data[0]', '$data[1]', CURRENT_TIMESTAMP);";

            mysql_query($import1) or die(mysql_error());

            # areas afetadas
            $areas = explode(',', $data[8]);

            for($i = 0; $i < count($areas); $i++){
                $area = "SELECT id FROM mps.areas WHERE nome='$areas[$i]'";
                $import2 = "INSERT INTO mps.inc_areas_afetadas (incidente, area) VALUES ('$uid', ($area));";

                mysql_query($import2) or die(mysql_error());
            }

            #aplicacoes afetadas
            $apps = explode(',', $data[9]);

            for($i = 0; $i < count($apps); $i++){
                $app = "SELECT id FROM mps.areas WHERE nome='$apps[$i]'";
                $import3 = "INSERT INTO mps.inc_aplicacoes_afetadas (incidente, aplicacao) VALUES ('$uid', ($app));";

                mysql_query($import3) or die(mysql_error());
            }

            #
        }
      
        #
        fclose($handle);
      
        #
        print "Importacao feita o/";
      
        //Visualizar formulário de transferência
    }
    else{
        print "Transferir novos arquivos CSV selecionando o arquivo e clicando no botão Upload<br />\n";
        print "<form enctype='multipart/form-data' action='importar2.php' method='post'>";
        print "Nome do arquivo para importar:<br />\n";
        print "<input size='50' type='file' name='filename'><br />\n";
        print "<input type='submit' name='submit' value='Upload'></form>";
     
    }
  
?>