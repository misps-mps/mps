<script language="Javascript">
    if(navigator.appName.indexOf('Internet Explorer')>0){
	   alert('Para acessar esta pagina e necessario o uso do Google Chrome, por favor, realize o download clicando em ok.')
        window.location.href = 'https://www.google.com/intl/pt-BR/chrome/browser/';
    }
    else{
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>

<!--------------------
LOGIN FORM
by: Amit Jakhu
www.amitjakhu.com
--------------------->

<!--META-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MPS // Login</title>

<!--STYLESHEETS-->
<link href="assets/form.css" rel="stylesheet" type="text/css" />

<!--SCRIPTS-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<!--Slider-in icons-->
<script type="text/javascript">
$(document).ready(function() {
	$(".username").focus(function() {
		$(".user-icon").css("left","-48px");
	});
	$(".username").blur(function() {
		$(".user-icon").css("left","0px");
	});
	
	$(".password").focus(function() {
		$(".pass-icon").css("left","-48px");
	});
	$(".password").blur(function() {
		$(".pass-icon").css("left","0px");
	});
});
</script>

</head>
<body>

<!--WRAPPER-->
<div id="wrapper">

	<!--SLIDE-IN ICONS-->
    <div class="user-icon"></div>
    <div class="pass-icon"></div>
    <!--END SLIDE-IN ICONS-->

<!--LOGIN FORM-->
<form name="login-form" class="login-form" action="validacao.php" method="post">

	<!--HEADER-->
    <div class="header">
    <!--TITLE--><center><h1>MPS //</h1><!--END TITLE-->
    <!--DESCRIPTION--><span>A partir deste momento seus acessos estão sendo computados e direcionadas para nossa base de dados. Seja prudente.</span><!--END DESCRIPTION-->
    </div>
    <!--END HEADER-->
	
	<!--CONTENT-->
    <div class="content">
	<!--USERNAME--><input name="usuario" type="text" class="input username" value="Usuário" onfocus="this.value=''" /><!--END USERNAME-->
    <!--PASSWORD--><input name="senha" type="password" class="input password" value="Senha" onfocus="this.value=''" /><!--END PASSWORD-->
    </div></center>
    <!--END CONTENT-->
    
    <!--FOOTER-->
    <div class="footer">
    <!--LOGIN BUTTON--><input type="submit" name="submit" value="Login" class="button" /><!--END LOGIN BUTTON-->
    <!--REGISTER BUTTON--><input type="submit" name="submit" value="Ajuda" class="register" /><!--END REGISTER BUTTON-->
    </div>
    <!--END FOOTER-->

</form>
<!--END LOGIN FORM-->

</div>
<!--END WRAPPER-->

<!--GRADIENT--><div class="gradient"></div><!--END GRADIENT-->

</body>
</html>
<!-- QUEMSOU: <?php echo $_SERVER['SERVER_ADDR'];?> -->