<?php include('header.php'); ?>
<script language="JavaScript">
function abrir(URL) {
  window.open(URL,'janela', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=yes, fullscreen=yes'); 
}
</script>
 

                <div class="span9" id="content">
                      <!-- morris stacked chart -->
				
                <div class="row-fluid" id="mapa-alagamentos">
                        <!-- block -->
                    <div class="block">
                        <div class="navbar navbar-inner block-header">
                            <div class="muted pull-left">Mapa Pontos de Alagamento</div>
                        </div>
                        <div class="block-content collapse in">
							<a href="javascript:abrir('views/mapa-alagamentos.php');">Visualizar em Tela Cheia </a>|<a href="#modal"> Inserir Ponto</a></p>	
							<iframe src="views/mapa-alagamentos.php" height="500px" width="100%" frameborder="0" scrolling="no"></iframe>
							<p>Importante lembrar que: As informa��es de alagamento disponibilizadas no website do CGE s�o fornecidas pela Companhia de Engenharia de Tr�fego (CET). As ocorr�ncias s�o registradas pessoalmente pelos agentes de tr�nsito da CET, que encaminham as informa��es para um sistema onde s�o contabilizados. � importante lembrar que o monitoramento de tr�nsito da CET � realizado com maior expressividade no Centro expandido e principais corredores de tr�fego. Portanto, � poss�vel que certas ocorr�ncias n�o sejam registradas no sistema.</p>

							<p><b>Classifica��o</p></b>

							<p><b>Transit�vel:</b> Significa que o ac�mulo de �guas n�o toma todas as pistas da via, e assim sendo, o tr�fego de ve�culos permanece liberado no local.</p>

							<p><b>Intransit�vel:</b> O alagamento toma todas as pistas da via, e impede totalmente o tr�nsito de ve�culos naquele local.</p>
							
                        </div>
                        <!-- /block -->
					</div>
					
                <div class="row-fluid" id="lista-alagamentos">
                        <!-- block -->
                    <div class="block">
                        <div class="navbar navbar-inner block-header">
                            <div class="muted pull-left">Lista Pontos de Alagamento</div>
                        </div>
                        <div class="block-content collapse in">
							<iframe src="views/table-alagamento.php" height="550px" width="100%" frameborder="0" scrolling="no"></iframe>
                        </div>
                        <!-- /block -->
					</div>

                </div>
				
            </div>
            <hr>

        </div>
		
		
		<div id="modal">
			<div class="modal-content">
				<div class="copy">
					<form class="form-horizontal" id="cadastroAlagamento" name="cadastroAlagamento" method="post" action="inserts/cadastro-alagamento.php">
					  <fieldset>
						<legend>Inserir Dados Pontos de Alagamento</legend>
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Data</label>
						  <div class="controls">
							<input type="text" class="span7 typeahead input" name="data" id="data" value="<?php echo date('d/m/Y')?>">
						  </div>
						</div>							
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Local </label>
						  <div class="controls">
							<input type="text" class="span7 typeahead input" name="local" data-provide="typeahead" data-items="4" data-source='["Madrugada", "Manh�", "Tarde", "Noite"]'>
						  </div>
						</div>    
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Referencia </label>
						  <div class="controls">
							<input type="text" class="span7 typeahead input" name="referencia" data-provide="typeahead" data-items="4" data-source='["Madrugada", "Manh�", "Tarde", "Noite"]'>
						  </div>
						</div>
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Latitude </label>
						  <div class="controls">
							<input type="text" class="span7 typeahead input" name="latitude" data-provide="typeahead" data-items="4" data-source='["Madrugada", "Manh�", "Tarde", "Noite"]'>
						  </div>
						</div>
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Longitude </label>
						  <div class="controls">
							<input type="text" class="span7 typeahead input" name="longitude" data-provide="typeahead" data-items="4" data-source='["Madrugada", "Manh�", "Tarde", "Noite"]'>
						  </div>
						</div>   		
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Zona </label>
						  <div class="controls">
							<select name="zona" id="selectError3" class="span7 typeahead input">
								<option>Norte</option>
								<option>Sul</option>
								<option>Sudeste</option>
								<option>Leste</option>
								<option>Oeste</option>
							</select>
						  </div>
						</div>
						<div class="control-group">
						  <label class="control-label" for="appendedInput">Situa��o </label>
						  <div class="controls">
							<select name="situacao" id="selectError3" class="span7 typeahead input">
								<option>Transit�vel</option>
								<option>Intransit�vel</option>
							</select>
						  </div>
						</div>   		
						<div class="control-group">
							<label class="control-label" for="appendedInput">Inicio</label>
							<div class="controls">
							  <div class="input-append">
								<input id="appendedInput" name="inicio" id="appendedInput" class="span8" type="text"><span class="add-on">Horas</span>
							  </div>
							</div>
						</div>	
						<div class="control-group">
							<label class="control-label" for="appendedInput">Fim</label>
							<div class="controls">
							  <div class="input-append">
								<input name="fim" id="appendedInput" class="span8" type="text"><span class="add-on">Horas</span>
							  </div>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Inserir</button><a href="#" class="btn">Fechar</a>
					  </fieldset>
					</form>
					<?php //Transferir o arquivo
					if (isset($_POST['submit'])) {
					  
						if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
							echo "<h1>" . "File ". $_FILES['filename']['name'] ." transferido com sucesso ." . "</h1>";
						}
					  
						//Importar o arquivo transferido para o banco de dados
						$handle = fopen($_FILES['filename']['tmp_name'], "r");
					  
						while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
							$conexao = mysql_connect("localhost", "F0116002", "gmatos.039");
							$banco="mps";
							$bd=mysql_select_db($banco, $conexao);
							$import="INSERT into alagamentos(data,endereco,zona,inicio,fim,situacao,latitude,longitude)values('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]')";
					  
							mysql_query($import) or die(mysql_error());
						}
					  
						fclose($handle);
					  
						print "Importa��o feita.";
					  
					//Visualizar formul�rio de transfer�ncia
					} else {
					  
						print "<form enctype='multipart/form-data' action='clima.php' method='post'>";
						print "<input size='50' type='file' name='filename'><br />\n";
						print "<input type='submit' name='submit' value='Upload'></form>";
					}
					?>								
				</div>		
			</div>
			<div class="overlay"></div>
		</div>		
        <!--/.fluid-container-->
        <link href="js/datepicker.css" rel="stylesheet" media="screen">
        <link href="js/uniform.default.css" rel="stylesheet" media="screen">
        <link href="js/chosen.min.css" rel="stylesheet" media="screen">

        <link href="js/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="js/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.uniform.min.js"></script>
        <script src="js/chosen.jquery.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>

        <script src="js/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="js/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="js/wizard/jquery.bootstrap.wizard.min.js"></script>


        <script src="css/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
    </body>

<?php include 'footer.php'?>