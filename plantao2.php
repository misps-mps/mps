<?php include('header2.php'); ?>

<script language="Javascript">
function showDiv(div){
	document.getElementById("rdm").className = "invisivel";
	document.getElementById("performance").className = "invisivel";
	document.getElementById("atualizacao").className = "invisivel";
	document.getElementById("greves").className = "invisivel";
	document.getElementById("quedas").className = "invisivel";

	document.getElementById("Andamento").className = "invisivel";
	document.getElementById("Iminente").className = "invisivel";
	document.getElementById("Normalizado").className = "invisivel";

	document.getElementById(div).className = "visivel";
}

function showImg(div){
	document.getElementById("Andamento").className = "invisivel";
	document.getElementById("Iminente").className = "invisivel";
	document.getElementById("Normalizado").className = "invisivel";

	document.getElementById(div).className = "visivel";
}

$(document).ready(function(){
  	$('.hide-sidebar').hide();
});
</script>
<style>
.invisivel { display: none; }
.visivel { visibility: visible; }
</style>
                <!--/span-->
                <div class="span12" id="content">

                    <div class="row-fluid">

                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">

	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar" id="teste"><a href='#' title="Hide Sidebar" rel='tooltip' id="teste">&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Plant�o Torre</a>	
	                                    </li>
	                                </ul>
                            </div>
                            <div class="block-content collapse in">
								<div width="100%">
									<div style="height:46px; background-image: url(images/logo.png); background-repeat:no-repeat;  position: absolute; width:100%;"></div>
									<div style="margin-left:6%; height:46px; width:100%; background-image: url(images/bg-top.png); color:#FFF; background-repeat:repeat-x; position fixed  top:auto; line-height: 5px; text-indent:10px;">
										<br><h4>Plant�o de Crise | MPS</h4>
									</div>
								</div>
								<hr>
								<div class="esquerda">
									<form class="form-horizontal" id="cadastroAlagamento" name="cadastroAlagamento" method="post" action="inserts/cadastro-boletim.php">
									  <fieldset>								
										<div class="control-group">
											<label class="control-label" for="appendedInput">Categoria</label>
											<div class="controls">
													<select name="categoria" onchange="showDiv(this.value);"  type="text" name="status" class="span7 typeahead input">
														<option value="performance">Indicadores de Performance</option>
														<option value="greves">Greves e Paralisa��es</option>
														<option value="quedas">Quedas Sist�micas</option>
														<option value="atualizacao">Implanta��es / Projetos</option>
														<option value="rdm">RDM - Requisi��o de Mudan�a</option>
													</select>
											</div>
										</div>									  
										<div class="control-group">
										  <label class="control-label" for="appendedInput">Status </label>
										  <div class="controls">
													<select type="text" name="status" class="span7 typeahead input"onchange="showImg(this.value);">
														<option value="Andamento">Em Andamento</option>
														<option value="Iminente">Iminente</option>
														<option value="Normalizado">Normalizado</option>
													</select>
													<img id="Andamento" class="invisivel" src="images/vermelho.png">
													<img id="Iminente" class="invisivel" src="images/amarelo.png">
													<img id="Normalizado" class="invisivel" src="images/verde.png">
										  </div>
										</div>
										<div class="control-group">
										  <label class="control-label" for="appendedInput">Assunto</label>
										  <div class="controls">
											<input type="text" name="assunto" class="span11 typeahead input"></input>
										  </div>
										</div>    
										<div class="control-group">
										  <label class="control-label" for="appendedInput">In�cio �s </label>
										  <div class="controls">
											<input type="text" name="inicio" class="span2 typeahead input"></input>
											&nbsp Fim �s &nbsp
											<input type="text" name="fim" class="span2 typeahead input"></input>
											&nbsp Tempo total de Impacto &nbsp
											<input type="text" name="duracao" class="span2 typeahead input"></input>
										  </div>										  
										</div>										
										<div class="control-group">
										  <label class="control-label" for="appendedInput">Resumo </label>
										  <div class="controls">
												<textarea id="textarea-1" rows="5"  type="text" name="resumo" class="span11 typeahead input"></textarea>
										  </div>
										</div>										
										<div class="control-group">
										  <label class="control-label" for="appendedInput">Poss�veis Impactos </label>
										  <div class="controls">
											<textarea type="text" rows="2" name="impactos" class="span11 typeahead input"></textarea>
										  </div>
										</div>
										<div class="control-group">
										  <label class="control-label" for="appendedInput">A��es em andamento ou propostas </label>
										  <div class="controls">
											<textarea type="text" rows="2" name="propostas" class="span11 typeahead input"></textarea>
										  </div>
										</div>		
										<div class="control-group">
										  <label class="control-label" for="appendedInput">Opera��es Impactadas </label>
										  <div class="controls">
											<textarea type="text" rows="2" name="operacoes" class="span11 typeahead input"></textarea>
										  </div>
										</div>											
										<div class="form-actions span11">
										  <button type="submit" class="btn btn-primary">Inserir</button>
										  <button type="reset" class="btn">Cancelar</button>
										</div>
									  </fieldset>
									</form>
								</div>
								<div class="direita" align="right">			
									<div id="performance" class="visivel" style="margin-right:50px;"><iframe scrolling="no" src="frames/def-performance.php" width="425px" height="700px" frameborder="0"></iframe></div>
									<div id="atualizacao" class="invisivel" style="margin-right:50px;"><iframe scrolling="no" src="frames/def-atualizacao.php" width="425px" height="700px" frameborder="0"></iframe></div>
									<div id="rdm" class="invisivel" style="margin-right:50px;"><iframe scrolling="no" src="frames/def-rdm.php" width="425px" height="700px" frameborder="0"></iframe></div>
									<div id="quedas" class="invisivel" style="margin-right:50px;"><iframe scrolling="no" src="frames/def-quedas.php" width="425px" height="700px" frameborder="0"></iframe></div>			
									<div id="greves" class="invisivel" style="margin-right:50px;"><iframe scrolling="no" src="frames/def-greves.php" width="425px" height="700px" frameborder="0"></iframe></div>										
								</div>	
							</div>
								
						</div>
                    </div>
                        <!-- /block -->
					
                    <div class="row-fluid">

                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">

	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Boletim</a>	
	                                    </li>
	                                </ul>
                            </div>
                            <div class="block-content collapse in" id="mydiv">
								<iframe src="views/boletins-graph.php" height="600px" width="100%" frameborder="0" allowtransparency="yes" scrolling="yes"></iframe>
							</div>
						</div>
						<!-- /block -->
					</div>				
