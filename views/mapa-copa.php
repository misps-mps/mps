<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head> 
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
<title>Porto Maps</title> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript" src="../vendors/download.js"></script>
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
<style type="text/css">
      html, body, #map {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
		left:10%;
        top: 5px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
      #panel2 {
        position: absolute;
		right:2%;
        top: 20%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }	  
</style>
    <script type="text/javascript">
    //<![CDATA[
      // this variable will collect the html which will eventually be placed in the side_bar 
      var side_bar_html = ""; 

      var gmarkers = [];
      var gicons = [];
      var map = null;
	  var directionsDisplay;
	  var directionsService = new google.maps.DirectionsService();;

	
var infowindow = new google.maps.InfoWindow(
  { 
    size: new google.maps.Size(150,50)
  });


gicons["red"] = new google.maps.MarkerImage("mmm_20_blue.png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(40, 40),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  // Marker sizes are expressed as a Size of X,Y
  // where the origin of the image (0,0) is located
  // in the top left of the image.
 
  // Origins, anchor positions and coordinates of the marker
  // increase in the X direction to the right and in
  // the Y direction down.

  var iconImage = new google.maps.MarkerImage('mm_20_blue.png',
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(40, 40),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
      // The shadow image is larger in the horizontal dimension
      // while the position and offset are the same as for the main image.
      new google.maps.Size(37, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(9, 34));
      // Shapes define the clickable region of the icon.
      // The type defines an HTML &lt;area&gt; element 'poly' which
      // traces out a polygon as a series of X,Y points. The final
      // coordinate closes the poly by connecting to the first
      // coordinate.
  var iconShape = {
      coord: [9,0,6,1,4,2,2,4,0,8,0,12,1,14,2,16,5,19,7,23,8,26,9,30,9,34,11,34,11,30,12,26,13,24,14,21,16,18,18,16,20,12,20,8,18,4,16,2,15,1,13,0],
      type: 'poly'
  };

function getMarkerImage(iconColor) {
   if ((typeof(iconColor)=="undefined") || (iconColor==null)) { 
      iconColor = "red"; 
   }
   if (!gicons[iconColor]) {
      gicons[iconColor] = new google.maps.MarkerImage("../images/marker_"+ iconColor +".png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(40, 40),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 6,20.
      new google.maps.Point(9, 34));
   } 
   return gicons[iconColor];

}

function category2color(category) {
   var color = "red";
   switch(category) {
     case "Estadio":    color = "Estadio";
                break;	
     case "Hotel":    color = "Hotel";
                break;	
     case "CT":    color = "CT";
                break;	
     case "porto":    color = "porto";
                break;	
     case "sucursal":    color = "porto";
                break;
     case "norte":    color = "person";
                break;	
     case "sul":    color = "person";
                break;	
     case "centro":    color = "person";
                break;	
     case "leste":    color = "person";
                break;					
     case "oeste":    color = "person";
                break;			
     case "grande":    color = "person";
                break;	
     default:   color = "red";
                break;
   }
   return color;
}		
	  gicons["Estadio"] = getMarkerImage(category2color("Estadio"));
	  gicons["Hotel"] = getMarkerImage(category2color("Hotel"));
	  gicons["CT"] = getMarkerImage(category2color("CT"));
	  gicons["porto"] = getMarkerImage(category2color("porto"));
	  gicons["sucursal"] = getMarkerImage(category2color("sucursal"));
	  gicons["norte"] = getMarkerImage(category2color("norte"));
	  gicons["sul"] = getMarkerImage(category2color("sul"));
	  gicons["centro"] = getMarkerImage(category2color("centro"));
	  gicons["leste"] = getMarkerImage(category2color("leste"));
	  gicons["oeste"] = getMarkerImage(category2color("oeste"));
	  gicons["grande"] = getMarkerImage(category2color("grande"));	  


      // A function to create the marker and set up the event window
function createMarker(latlng,name,html,category) {
    var contentString = html;
    var marker = new google.maps.Marker({
        position: latlng,
        icon: gicons[category],
        shadow: iconShadow,
        map: map,
        title: name,
        zIndex: Math.round(latlng.lat()*-100000)<<5
        });
        // === Store the category and name info as a marker properties ===
        marker.mycategory = category;                                 
        marker.myname = name;
        gmarkers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
        });
}

      // == shows all markers of a particular category, and ensures the checkbox is checked ==
      function show(category) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].mycategory == category) {
            gmarkers[i].setVisible(true);
          }
        }
        // == check the checkbox ==
        document.getElementById(category+"box").checked = true;
      }

      // == hides all markers of a particular category, and ensures the checkbox is cleared ==
      function hide(category) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].mycategory == category) {
            gmarkers[i].setVisible(false);
          }
        }
        // == clear the checkbox ==
        document.getElementById(category+"box").checked = false;
        // == close the info window, in case its open on a marker that we just hid
        infowindow.close();
      }

      // == a checkbox has been clicked ==
      function boxclick(box,category) {
        if (box.checked) {
          show(category);
        } else {
          hide(category);
        }
        // == rebuild the side bar
        makeSidebar();
      }

      function myclick(i) {
        google.maps.event.trigger(gmarkers[i],"click");
      }
	  
function calcRoute() {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}	  

  function initialize() {
   directionsDisplay = new google.maps.DirectionsRenderer();
    var myOptions = {
      zoom: 11,
      center: new google.maps.LatLng(-23.5160205,-46.6649511),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);


    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
        });
		
	directionsDisplay.setMap(map);
	
  var transitLayer = new google.maps.TransitLayer();
  transitLayer.setMap(map);			


      // Read the data
      downloadUrl("categories.xml", function(doc) {
  var xml = xmlParse(doc);
  var markers = xml.documentElement.getElementsByTagName("marker");
          
        for (var i = 0; i < markers.length; i++) {
          // obtain the attribues of each marker
          var lat = parseFloat(markers[i].getAttribute("lat"));
          var lng = parseFloat(markers[i].getAttribute("lng"));
          var point = new google.maps.LatLng(lat,lng);
          var address = markers[i].getAttribute("address");
          var name = markers[i].getAttribute("name");
          var html = "<b>"+name+"<\/b><p>"+address;
          var category = markers[i].getAttribute("category");
          // create the marker
          var marker = createMarker(point,name,html,category);
        }
		
        // == show or hide the categories initially ==
		hide("sucursal");
		hide("Estadio");
		hide("Hotel");
		hide("CT");
		show("Porto");
		hide("norte");
		hide("centro");
		hide("sul");
		hide("leste");
		hide("oeste");
		hide("grande");		
      });
    }

    //]]>
    </script>
  </head>
<body style="margin:0px; padding:0px;" onload="initialize()"> 

    <div id="map"></div>
    <form action="#" id="panel">
	  Estadio <input type="checkbox" id="Estadiobox" onclick="boxclick(this,'Estadio')" /> |
	  CT <input type="checkbox" id="CTbox" onclick="boxclick(this,'CT')" /> | 
	  Hotel <input type="checkbox" id="Hotelbox" onclick="boxclick(this,'Hotel')" />  |
	  Porto <input type="checkbox" id="Portobox" onclick="boxclick(this,'porto')" />  |
	  Sucursal <input type="checkbox" id="sucursalbox" onclick="boxclick(this,'sucursal')" /> | 
		<strong id="input">Ponto de Saida: </strong>
		<select id="start" class="input" onchange="calcRoute();">
				<?php
					$user="F0116002";$pass="gmatos.039";$host="localhost";$banco="mps";
					$con=mysql_connect($host,$user,$pass)or die ("Erro na conexão com o host.");
					$bd=mysql_select_db($banco, $con)or die("Erro na seleção do Banco de Dados.");
					$query=mysql_query("SELECT * FROM copa WHERE categoria='CT' OR categoria ='Estadio' OR categoria='Hotel' ORDER BY nome");
					echo"<option value=\"\"></option>";
					while($ver=mysql_fetch_array($query)){		
							echo "<option value=\"";echo $ver['latitude'];echo",";echo $ver['longitude'];echo"\">";echo $ver['nome'];echo", ";echo $ver['categoria'];echo"</option>";		
						}
				?>
		  </select>
		<strong id="input">Ponto de chegada: </strong>
		<select id="end"  class="input" onchange="calcRoute();">
				<?php
					$user="F0116002";$pass="gmatos.039";$host="localhost";$banco="mps";
					$con=mysql_connect($host,$user,$pass)or die ("Erro na conexão com o host.");
					$bd=mysql_select_db($banco, $con)or die("Erro na seleção do Banco de Dados.");
					$query=mysql_query("SELECT * FROM copa WHERE categoria='CT' OR categoria ='Estadio' OR categoria='Hotel' ORDER BY nome");
					echo"<option value=\"\"></option>";
					while($ver=mysql_fetch_array($query)){		
							echo "<option value=\"";echo $ver['latitude'];echo",";echo $ver['longitude'];echo"\">";echo $ver['nome'];echo", ";echo $ver['categoria'];echo"</option>";		
						}
				?>
		</select>	  
    </form>  
	
	<form action="#" id="panel2">
		Norte <input type="checkbox" id="nortebox" onclick="boxclick(this,'norte')" /><br>
		Centro <input type="checkbox" id="centrobox" onclick="boxclick(this,'centro')" /><br>
		Sul <input type="checkbox" id="sulbox" onclick="boxclick(this,'sul')" /><br>
		Leste <input type="checkbox" id="lestebox" onclick="boxclick(this,'leste')" /><br>
		Oeste <input type="checkbox" id="oestebox" onclick="boxclick(this,'oeste')" /><br>
		Grande SP <input type="checkbox" id="grandebox" onclick="boxclick(this,'grande')" />
	</form>
  </body>

</html>