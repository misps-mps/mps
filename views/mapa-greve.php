<!DOcentroYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head> 
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
<title>Porto Maps</title> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript" src="../vendors/download.js"></script>
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">

<style type="text/css">
      html, body, #map {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
		left:10%;
        top: 5px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
      #lateral {
        position: absolute;
        top: 20%;
		left: 2%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      } 
	  
      #lateral2 {
        position: absolute;
        top: 60%;
		left: 2%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      } 
	  
	  .fonte{
		font-size:12px;
	  }
	  
</style>
    <script type="text/javascript">
    //<![CDATA[
      // this variable will collect the html which will eventually be placed in the side_bar 
      var side_bar_html = ""; 

      var gmarkers = [];
      var gicons = [];
      var map = null;
	  var directionsDisplay;
	  var directionsService = new google.maps.DirectionsService();;

  			var citymap = {};
			citymap['Alvim'] = {
			  center: new google.maps.LatLng(-23.561518, -46.656009),
			  population: 30000
			};
			citymap['Tucuruvi'] = {
			  center: new google.maps.LatLng(-23.479813, -46.604362),
			  population: 30000
			}
			citymap['Jabaquara '] = {
			  center: new google.maps.LatLng(-23.645798, -46.638935),
			  population: 30000
			}
			citymap['Tatuapé'] = {
			  center: new google.maps.LatLng(-23.560478, -46.574787),
			  population: 30000
			}
			var cityCircle;	  
	  
var infowindow = new google.maps.InfoWindow(
  { 
    size: new google.maps.Size(160,60)
  });

gicons["red"] = new google.maps.MarkerImage("mmm_20_blue.png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(60, 60),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  // Marker sizes are expressed as a Size of X,Y
  // where the origin of the image (0,0) is located
  // in the top left of the image.
 
  // Origins, anchor positions and coordinates of the marker
  // increase in the X direction to the right and in
  // the Y direction down.

  var iconImage = new google.maps.MarkerImage('mm_20_blue.png',
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(60, 60),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow60.png',
      // The shadow image is larger in the horizontal dimension
      // while the position and offset are the same as for the main image.
      new google.maps.Size(37, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(9, 34));
      // Shapes define the clickable region of the icon.
      // The type defines an HTML &lt;area&gt; element 'poly' which
      // traces out a polygon as a series of X,Y points. The final
      // coordinate closes the poly by connecting to the first
      // coordinate.
  var iconShape = {
      coord: [9,0,6,1,4,2,2,4,0,8,0,12,1,14,2,16,5,19,7,23,8,26,9,30,9,34,11,34,11,30,12,26,13,24,14,21,16,18,18,16,20,12,20,8,18,4,16,2,15,1,13,0],
      type: 'poly'
  };

function getMarkerImage(iconColor) {
   if ((typeof(iconColor)=="undefined") || (iconColor==null)) { 
      iconColor = "red"; 
   }
   if (!gicons[iconColor]) {
      gicons[iconColor] = new google.maps.MarkerImage("../images/marker_"+ iconColor +".png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(60, 60),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 6,20.
      new google.maps.Point(9, 34));
   } 
   return gicons[iconColor];

}

function category2color(category) {
   var color = "red";
   switch(category) {
     case "protesto":    color = "protesto";
                break;	
				
     default:   color = "red";
                break;
   }
   return color;
}		
	  gicons["protesto"] = getMarkerImage(category2color("protesto"));

	 
      // A function to create the marker and set up the event window
function createMarker(latlng,name,html,category) {
    var contentString = html;
    var marker = new google.maps.Marker({
        position: latlng,
        icon: gicons[category],
        shadow: iconShadow,
        map: map,
        title: name,
        zIndex: Math.round(latlng.lat()*-100000)<<5
        });
        // === Store the category and name info as a marker properties ===
        marker.mycategory = category;                                 
        marker.myname = name;
        gmarkers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
        });
}

      // == shows all markers of a particular category, and ensures the checkbox is checked ==
      function show(category) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].mycategory == category) {
            gmarkers[i].setVisible(true);
          }
        }
        // == check the checkbox ==
        document.getElementById(category+"box").checked = true;
      }

      // == hides all markers of a particular category, and ensures the checkbox is cleared ==
      function hide(category) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].mycategory == category) {
            gmarkers[i].setVisible(false);
          }
        }
        // == clear the checkbox ==
        document.getElementById(category+"box").checked = false;
        // == close the info window, in case its open on a marker that we just hid
        infowindow.close();
      }

      // == a checkbox has been clicked ==
      function boxclick(box,category) {
        if (box.checked) {
          show(category);
        } else {
          hide(category);
        }
        // == rebuild the side bar
        makeSidebar();
      }

      function myclick(i) {
        google.maps.event.trigger(gmarkers[i],"click");
      }
	  
function calcRoute() {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}	  

  function initialize() {
   directionsDisplay = new google.maps.DirectionsRenderer();
    var myOptions = {
      zoom: 11,
      center: new google.maps.LatLng(-23.5160205,-46.6649511),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);


    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
        });
		
	directionsDisplay.setMap(map);

      // Read the data
      downloadUrl("greve.xml", function(doc) {
  var xml = xmlParse(doc);
  var markers = xml.documentElement.getElementsByTagName("marker");
          
        for (var i = 0; i < markers.length; i++) {
          // obtain the attribues of each marker
          var lat = parseFloat(markers[i].getAttribute("lat"));
          var lng = parseFloat(markers[i].getAttribute("lng"));
          var point = new google.maps.LatLng(lat,lng);
          var address = markers[i].getAttribute("address");
          var name = markers[i].getAttribute("name");
          var html = "<b>"+name+"<\/b><p>"+address;
          var category = markers[i].getAttribute("category");
          // create the marker
          var marker = createMarker(point,name,html,category);
        }

// for (var city in citymap) {
    
    // var populationOptions = {
      // strokeColor: '#0000FF',
      // strokeOpacity: 0.23,
      // strokeWeight: 100,
      // fillColor: '#FF0000',
      // fillOpacity: 0.23,
      // map: map,
      // center: citymap[city].center,
      // radius: citymap[city].population / 20 * 2
    // };
    // cityCircle = new google.maps.Circle(populationOptions);
  // }
	
        // == show or hide the categories initially ==
		show("porto");
		show("vanmetro");
		hide("protesto");
		hide("centro");
		hide("sul");
		hide("leste");
		hide("oeste");
		hide("grande");
		hide("terminal");
		hide("azul");
		hide("verde");
		hide("amarela");
		hide("vermelha");
		hide("azul");
		hide("lilas");
		hide("vantrem");
      });
    }

    //]]>
    </script>
  </head>
<body style="margin:0px; padding:0px;" onload="initialize()"> 

    <div id="map"></div>
</body>

</html>




