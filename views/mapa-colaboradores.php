<!DOcentroYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head> 
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
<title>Porto Maps</title> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript" src="../vendors/download.js"></script>
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">

<style type="text/css">
      html, body, #map {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
		left:10%;
        top: 5px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
      #lateral {
        position: absolute;
        top: 20%;
		left: 2%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      } 
	  
      #lateral2 {
        position: absolute;
        top: 50%;
		left: 2%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      } 
	  
	  .fonte{
		font-size:12px;
	  }
	  
</style>
    <script type="text/javascript">
    //<![CDATA[
      // this variable will collect the html which will eventually be placed in the side_bar 
      var side_bar_html = ""; 

      var gmarkers = [];
      var gicons = [];
      var map = null;
	  var directionsDisplay;
	  var directionsService = new google.maps.DirectionsService();;

  			var citymap = {};
			citymap['Alvim'] = {
			  center: new google.maps.LatLng(-23.5398103, -46.4827742),
			  population: 30000
			};
			citymap['Tucuruvi'] = {
			  center: new google.maps.LatLng(-23.479813, -46.604362),
			  population: 30000
			}
			citymap['Jabaquara '] = {
			  center: new google.maps.LatLng(-23.645798, -46.638935),
			  population: 30000
			}
			citymap['Tatuapé'] = {
			  center: new google.maps.LatLng(-23.540478, -46.574787),
			  population: 30000
			}
			var cityCircle;	  
	  
var infowindow = new google.maps.InfoWindow(
  { 
    size: new google.maps.Size(150,50)
  });

gicons["red"] = new google.maps.MarkerImage("mmm_20_blue.png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(40, 40),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  // Marker sizes are expressed as a Size of X,Y
  // where the origin of the image (0,0) is located
  // in the top left of the image.
 
  // Origins, anchor positions and coordinates of the marker
  // increase in the X direction to the right and in
  // the Y direction down.

  var iconImage = new google.maps.MarkerImage('mm_20_blue.png',
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(40, 40),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
      // The shadow image is larger in the horizontal dimension
      // while the position and offset are the same as for the main image.
      new google.maps.Size(37, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(9, 34));
      // Shapes define the clickable region of the icon.
      // The type defines an HTML &lt;area&gt; element 'poly' which
      // traces out a polygon as a series of X,Y points. The final
      // coordinate closes the poly by connecting to the first
      // coordinate.
  var iconShape = {
      coord: [9,0,6,1,4,2,2,4,0,8,0,12,1,14,2,16,5,19,7,23,8,26,9,30,9,34,11,34,11,30,12,26,13,24,14,21,16,18,18,16,20,12,20,8,18,4,16,2,15,1,13,0],
      type: 'poly'
  };

function getMarkerImage(iconColor) {
   if ((typeof(iconColor)=="undefined") || (iconColor==null)) { 
      iconColor = "red"; 
   }
   if (!gicons[iconColor]) {
      gicons[iconColor] = new google.maps.MarkerImage("../images/marker_"+ iconColor +".png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(40, 40),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 6,20.
      new google.maps.Point(9, 34));
   } 
   return gicons[iconColor];

}

function category2color(category) {
   var color = "red";
   switch(category) {
     case "norte":    color = "person";
                break;	
     case "sul":    color = "person";
                break;	
     case "centro":    color = "person";
                break;	
     case "person":    color = "person";
                break;	
     case "porto":    color = "porto";
                break;	
     case "vanmetro":    color = "van";
                break;	
     case "leste":    color = "person";
                break;					
     case "oeste":    color = "person";
                break;			
     case "grande":    color = "person";
                break;	
     case "trem":    color = "trem";
                break;		
     case "coral":    color = "coral";
                break;	
     case "diamante":    color = "diamante";
                break;	
     case "turquesa":    color = "turquesa";
                break;	
     case "rubi":    color = "rubi";
                break;	
     case "esmeralda":    color = "esmeralda";
                break;					
     case "terminal":    color = "terminal";
                break;	

     case "vermelha":    color = "vermelha";
                break;
     case "verde":    color = "verde";
                break;	
     case "amarela":    color = "amarela";
                break;	
     case "azul":    color = "azul";
                break;	
     case "lilas":    color = "lilas";
                break;		
     case "lamarela":    color = "lamarela";
                break;	
     case "llilas":    color = "llilas";
                break;	
     case "lazul":    color = "lazul";
                break;	
     case "lvermelha":    color = "lvermelha";
                break;		
     case "lverde":    color = "lverde";
                break;					
     case "vantrem":    color = "van";
                break;	
				
     default:   color = "red";
                break;
   }
   return color;
}		
	  gicons["norte"] = getMarkerImage(category2color("norte"));
	  gicons["sul"] = getMarkerImage(category2color("sul"));
	  gicons["centro"] = getMarkerImage(category2color("centro"));
	  gicons["person"] = getMarkerImage(category2color("person"));
	  gicons["porto"] = getMarkerImage(category2color("porto"));
	  gicons["vanmetro"] = getMarkerImage(category2color("vanmetro"));
	  gicons["leste"] = getMarkerImage(category2color("leste"));
	  gicons["oeste"] = getMarkerImage(category2color("oeste"));
	  gicons["grande"] = getMarkerImage(category2color("grande"));
	  gicons["trem"] = getMarkerImage(category2color("trem"));
	  gicons["coral"] = getMarkerImage(category2color("coral"));
	  gicons["diamante"] = getMarkerImage(category2color("diamante"));
	  gicons["turquesa"] = getMarkerImage(category2color("turquesa"));
	  gicons["rubi"] = getMarkerImage(category2color("rubi"));
	  gicons["esmeralda"] = getMarkerImage(category2color("esmeralda"));
	  gicons["terminal"] = getMarkerImage(category2color("terminal"));
	  gicons["verde"] = getMarkerImage(category2color("verde"));
	  gicons["amarela"] = getMarkerImage(category2color("amarela"));
	  gicons["vermelha"] = getMarkerImage(category2color("vermelha"));
	  gicons["azul"] = getMarkerImage(category2color("azul"));
	  gicons["lilas"] = getMarkerImage(category2color("lilas"));
	  gicons["llilas"] = getMarkerImage(category2color("llilas"));
	  gicons["lverde"] = getMarkerImage(category2color("lverde"));
	  gicons["lvermelha"] = getMarkerImage(category2color("lvermelha"));
	  gicons["lazul"] = getMarkerImage(category2color("lazul"));
	  gicons["lamarela"] = getMarkerImage(category2color("lamarela"));
	  gicons["vantrem"] = getMarkerImage(category2color("vantrem"));
	  
	  
	 
      // A function to create the marker and set up the event window
function createMarker(latlng,name,html,category) {
    var contentString = html;
    var marker = new google.maps.Marker({
        position: latlng,
        icon: gicons[category],
        shadow: iconShadow,
        map: map,
        title: name,
        zIndex: Math.round(latlng.lat()*-100000)<<5
        });
        // === Store the category and name info as a marker properties ===
        marker.mycategory = category;                                 
        marker.myname = name;
        gmarkers.push(marker);

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
        });
}

      // == shows all markers of a particular category, and ensures the checkbox is checked ==
      function show(category) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].mycategory == category) {
            gmarkers[i].setVisible(true);
          }
        }
        // == check the checkbox ==
        document.getElementById(category+"box").checked = true;
      }

      // == hides all markers of a particular category, and ensures the checkbox is cleared ==
      function hide(category) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].mycategory == category) {
            gmarkers[i].setVisible(false);
          }
        }
        // == clear the checkbox ==
        document.getElementById(category+"box").checked = false;
        // == close the info window, in case its open on a marker that we just hid
        infowindow.close();
      }

      // == a checkbox has been clicked ==
      function boxclick(box,category) {
        if (box.checked) {
          show(category);
        } else {
          hide(category);
        }
        // == rebuild the side bar
        makeSidebar();
      }

      function myclick(i) {
        google.maps.event.trigger(gmarkers[i],"click");
      }
	  
function calcRoute() {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}	  

  function initialize() {
   directionsDisplay = new google.maps.DirectionsRenderer();
    var myOptions = {
      zoom: 11,
      center: new google.maps.LatLng(-23.5160205,-46.6649511),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);


    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
        });
		
	directionsDisplay.setMap(map);
	
  var transitLayer = new google.maps.TransitLayer();
  transitLayer.setMap(map);			

      // Read the data
      downloadUrl("colaboradores.xml", function(doc) {
  var xml = xmlParse(doc);
  var markers = xml.documentElement.getElementsByTagName("marker");
          
        for (var i = 0; i < markers.length; i++) {
          // obtain the attribues of each marker
          var lat = parseFloat(markers[i].getAttribute("lat"));
          var lng = parseFloat(markers[i].getAttribute("lng"));
          var point = new google.maps.LatLng(lat,lng);
          var address = markers[i].getAttribute("address");
          var name = markers[i].getAttribute("name");
          var html = "<b>"+name+"<\/b><p>"+address;
          var category = markers[i].getAttribute("category");
          // create the marker
          var marker = createMarker(point,name,html,category);
        }
	
        // == show or hide the categories initially ==
		show("porto");
		show("vanmetro");
		hide("norte");
		hide("centro");
		hide("sul");
		hide("leste");
		hide("oeste");
		hide("grande");
		hide("terminal");
		hide("azul");
		hide("verde");
		hide("amarela");
		hide("vermelha");
		hide("azul");
		hide("lilas");
		hide("vantrem");
      });
    }

    //]]>
    </script>
  </head>
<body style="margin:0px; padding:0px;" onload="initialize()"> 

    <div id="map"></div>
    <form action="#" id="panel" class="fonte">
	  Porto <input type="checkbox" id="portobox" onclick="boxclick(this,'porto')" />  |
	  Van para greve Metro <input type="checkbox" id="vanmetrobox" onclick="boxclick(this,'vanmetro')" />  |
	  Van para greve Trem <input type="checkbox" id="vantrembox" onclick="boxclick(this,'vantrem')" />  |
	  Terminal <input type="checkbox" id="terminalbox" onclick="boxclick(this,'terminal')" />  |
		<strong id="input">Ponto de Saida: </strong>
		<select id="start" class="input fonte" onchange="calcRoute();">
			<option value="-23.655517,-46.717503">Santo Amaro</option>
			<option value="-23.530236,-46.774909">Osasco</option>
			<option value="-23.5342126,-46.3101918">Suzano</option>
			<option value="-23.656442,-46.524022">Santo André</option>
			<option value="-23.495391,-46.713719">Pirituba</option>
			<option value="-23.3265642,-46.7253487">Franco da Rocha</option>
			<option value="-23.543194,-46.416286">Guaianazes</option>
			<option value="-23.538345, -46.469650">Itaquera</option>
			<option value="-23.479813, -46.604362">Tucuruvi</option>
			<option value="-23.645798, -46.638935">Jabaquara</option>
			<option value="-23.540478, -46.574787">Tatuapé</option>

		</select>
		<strong id="input">Ponto de chegada: </strong>
		<select id="end"  class="input fonte" onchange="calcRoute();">
			<option value="-23.546347,-46.635239">Porto Seguro - Largo Do cafe</option>
			<option value="-23.531698,-46.649948">Porto Seguro - Barao de Limeira</option>
			<option value="-23.527172,-46.668827">Porto Seguro - Barra Funda</option>
			<option value="-23.532228,-46.642532">Porto Seguro - Glete</option>
			<option value="-23.541788,-46.640793">Porto Seguro - TIVIT</option>
		</select>
    </form>
	
	<form id="lateral">
	<table  class="fonte">
		<tr>
		  <td> <input type="checkbox" id="nortebox" onclick="boxclick(this,'norte')" /></td><td>Norte - 677 (24%)</td>
		</tr> 
		<tr>
		  <td> <input type="checkbox" id="centrobox" onclick="boxclick(this,'centro')" /></td><td>Centro - 120 (4%)</td>
		</tr>  
		<tr>
		  <td> <input type="checkbox" id="sulbox" onclick="boxclick(this,'sul')" /></td><td>Sul - 232 (8%)</td>
		</tr>
		<tr>	
		  <td> <input type="checkbox" id="lestebox" onclick="boxclick(this,'leste')" /></td><td>Leste - 1018 (36%)</td>
		</tr>
		<tr>
		  <td> <input type="checkbox" id="oestebox" onclick="boxclick(this,'oeste')" /></td><td>Oeste - 193 (7%)</td>
		</tr>
		<tr>
		  <td> <input type="checkbox" id="grandebox" onclick="boxclick(this,'grande')" /></td><td>Grande SP - 620 (22%)</td>
		</tr>
	</table>
	</form>
	
	<form id="lateral2">
	<table  class="fonte">
		<tr>
		  <td> <input type="checkbox" id="amarelabox" onclick="boxclick(this,'amarela')" /></td><td>Linha Amarela</td>
		</tr> 
		<tr>
		  <td> <input type="checkbox" id="verdebox" onclick="boxclick(this,'verde')" /></td><td>Linha Verde</td>
		</tr>  
		<tr>
		  <td> <input type="checkbox" id="lilasbox" onclick="boxclick(this,'lilas')" /></td><td>Linha Lilas</td>
		</tr>
		<tr>	
		  <td> <input type="checkbox" id="azulbox" onclick="boxclick(this,'azul')" /></td><td>Linha Azul</td>
		</tr>
		<tr>
		  <td> <input type="checkbox" id="vermelhabox" onclick="boxclick(this,'vermelha')" /></td><td>Linha Verrmelha</td>
		</tr>
	</table>
	</form>

</body>

</html>




