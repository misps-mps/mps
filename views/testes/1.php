
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<style>
	    html, body, #map {
        height: 100%;
        margin: 0px;
        padding: 0px
		}
      #panel {
        position: absolute;
        top: 5px;
		width: 75%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }		
	</style>	
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Google Maps JavaScript API Example</title>
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAUcHQ2ldBNAAr0jS88qEedBSItM6FQrLOEZkwRIcbYFrPk3yJGBSa_-BBB2_i0-BidwMJgkf4qz0ucw&hl"type="text/javascript"></script>

    <script type="text/javascript">
    //<![CDATA[

	var directionsDisplay;
	var directionsService = new google.maps.DirectionsService();	

    var iconEstadio = new GIcon(); 
    iconEstadio.image = '../images/Estadio.png';
    iconEstadio.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconEstadio.iconSize = new GSize(12, 20);
    iconEstadio.shadowSize = new GSize(22, 20);
    iconEstadio.iconAnchor = new GPoint(6, 20);
    iconEstadio.infoWindowAnchor = new GPoint(5, 1);

    var iconHotel = new GIcon(); 
    iconHotel.image = '../images/Hotel.png';
    iconHotel.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconHotel.iconSize = new GSize(12, 20);
    iconHotel.shadowSize = new GSize(22, 20);
    iconHotel.iconAnchor = new GPoint(6, 20);
    iconHotel.infoWindowAnchor = new GPoint(5, 1);
	
    var iconPorto = new GIcon(); 
    iconPorto.image = '../images/Porto.png';
    iconPorto.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconPorto.iconAnchor = new GPoint(6, 20);
    iconPorto.infoWindowAnchor = new GPoint(5, 1);	
	
    var iconVan = new GIcon(); 
    iconVan.image = '../images/bus.png';
    iconVan.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconVan.iconAnchor = new GPoint(6, 20);
    iconVan.infoWindowAnchor = new GPoint(5, 1);		
	
    var iconCT = new GIcon(); 
    iconCT.image = '../images/CT.png';
    iconCT.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconCT.iconSize = new GSize(12, 20);
    iconCT.shadowSize = new GSize(22, 20);
    iconCT.iconAnchor = new GPoint(6, 20);
    iconCT.infoWindowAnchor = new GPoint(5, 1);

    var iconPerson = new GIcon(); 
    iconPerson.image = '../images/person.png';
    iconPerson.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconPerson.iconSize = new GSize(12, 20);
    iconPerson.shadowSize = new GSize(22, 20);
    iconPerson.iconAnchor = new GPoint(6, 20);
    iconPerson.infoWindowAnchor = new GPoint(5, 1);	
	
    var iconSucursal = new GIcon(); 
    iconSucursal.image = '../images/porto.png';
    iconSucursal.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconSucursal.iconSize = new GSize(12, 20);
    iconSucursal.shadowSize = new GSize(22, 20);
    iconSucursal.iconAnchor = new GPoint(6, 20);
    iconSucursal.infoWindowAnchor = new GPoint(5, 1);		

    var customIcons = [];
    customIcons["Estadio"] = iconEstadio;
    customIcons["Hotel"] = iconHotel;
	customIcons["porto"] = iconPorto;
	customIcons["van"] = iconVan;	
	customIcons["CT"] = iconCT;
	customIcons["person"] = iconPerson;
	customIcons["sucursal"] = iconPerson;
    var markerGroups = { "Estadio": [], "Hotel": [], "porto": [], "van": [], "CT": [], "person": [], "sucursal": []};
		   

    function load() {
      if (GBrowserIsCompatible()) {
	  directionsDisplay = new google.maps.DirectionsRenderer();
        var map = new GMap2(document.getElementById("map"));
        map.setCenter(new GLatLng(-14.2400732, -53.1805018), 3);
		
        GDownloadUrl("2.xml", function(data) {
          var xml = GXml.parse(data);
          var markers = xml.documentElement.getElementsByTagName("marker");
          for (var i = 0; i < markers.length; i++) {
            var name = markers[i].getAttribute("name");
            var address = markers[i].getAttribute("address");
            var type = markers[i].getAttribute("type");
            var point = new GLatLng(parseFloat(markers[i].getAttribute("lat")),
                                    parseFloat(markers[i].getAttribute("lng")));
            var marker = createMarker(point, name, address, type);
            map.addOverlay(marker);
          }
		  
		  calcRoute();
		for (var city in citymap) {
    
			var populationOptions = {
			  strokeColor: '#0000FF',
			  strokeOpacity: 0.23,
			  strokeWeight: 100,
			  fillColor: '#FF0000',
			  fillOpacity: 0.23,
			  map: map,
			  center: citymap[city].center,
			  radius: citymap[city].population / 20 * 2
			};
			cityCircle = new google.maps.Circle(populationOptions);
		  }	  
        });
      }
    }

    function createMarker(point, name, address, type) {
      var marker = new GMarker(point, customIcons[type]);
      markerGroups[type].push(marker);
      var html = "<b>" + name + "</b> <br/>" + address;
      GEvent.addListener(marker, 'click', function() {
        marker.openInfoWindowHtml(html);
      });
      return marker;
    }
    function toggleGroup(type) {
      for (var i = 0; i < markerGroups[type].length; i++) {
        var marker = markerGroups[type][i];
        if (marker.isHidden()) {
          marker.show();
        } else {
          marker.hide();
        }
      } 
    }
	

    //]]>
  </script>
  </head>
  <body style="font-family:Arial, sans serif" onload="load()" onunload="GUnload()">
    <div id="map"></div>
    <div id="panel"><center>
   <input type="checkbox" id="restaurantCheckbox" onclick="toggleGroup('Estadio')" CHECKED />  
   Estadio

   <input type="checkbox" id="barCheckbox" onclick="toggleGroup('Hotel')" CHECKED/>  
   Hotel

   <input type="checkbox" id="barCheckbox" onclick="toggleGroup('CT')" CHECKED/>  
   CT

   <input type="checkbox" id="barCheckbox" onclick="toggleGroup('porto')" CHECKED/>  
   Porto

   <input type="checkbox" id="barCheckbox" onclick="toggleGroup('van')" CHECKED/>  
   Van

   <input type="checkbox" id="barCheckbox" onclick="toggleGroup('person')" CHECKED/>  
   Colaboradores 
   
   <input type="checkbox" id="barCheckbox" onclick="toggleGroup('sucursal')" CHECKED/>  
   Sucursal    
   </center></div>
  </body>
</html>
