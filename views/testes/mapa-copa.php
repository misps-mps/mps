
<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Porto Maps</title>

<style>
      html, body, #map {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
        top: 5px;
        left: 50%;
        margin-left: -180px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
    </style>
	
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBT914zwL2hP1JZT7u1xVPTMNjj6syEoJ8&sensor=false"></script>
    <script type="text/javascript">
	
	var directionsDisplay;
	var directionsService = new google.maps.DirectionsService();	
	
	function initialize() {
	 directionsDisplay = new google.maps.DirectionsRenderer();
	  var mapOptions = {
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: haight
	  }
	  map = new google.maps.Map(document.getElementById('map'), mapOptions);
	  
	}

	function calcRoute() {
	var start = document.getElementById('start').value;
	var end = document.getElementById('end').value;
	var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.DirectionsTravelMode.DRIVING	  
		  
	  };
	  directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
		  directionsDisplay.setDirections(response);
		}
	  });
	}
	
    var customIcons = {
      person: {
        icon: '../images/mm_20_blue.png',
        shadow: '../images/mm_20_shadow.png'
      },
      CT: {
        icon: '../images/CT.png',
        shadow: '../../images/mm_20_shadow.png'
      },
	  Estadio: {
        icon: '../images/Estadio.png',
        shadow: '../../images/mm_20_shadow.png'
      },
	  Hotel: {
        icon: '../images/Hotel.png',
        shadow: '../../images/mm_20_shadow.png'
      },
	  porto: {
        icon: '../images/porto.png',
        shadow: '../../images/mm_20_shadow.png'
      },
	  van: {
        icon: '../images/start.png',
        shadow: '../../images/mm_20_shadow.png'
      },	  
    };
	
		    var citymap = {};
			 citymap['Itaquera'] = {
			   center: new google.maps.LatLng(-23.538345, -46.469650),
			   population: 30000
			 };
			 citymap['Tucuruvi'] = {
			   center: new google.maps.LatLng(-23.479813, -46.604362),
			   population: 30000
			 }
			 citymap['Jabaquara '] = {
			   center: new google.maps.LatLng(-23.645798, -46.638935),
			   population: 30000
			 }
			 citymap['Tatuap�'] = {
			   center: new google.maps.LatLng(-23.540478, -46.574787),
			   population: 30000
			 }
			 citymap['Santo Amaro'] = {
			   center: new google.maps.LatLng(-23.655479, -46.717064),
			   population: 30000
			 }
			 var cityCircle;

    function load() {
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(-14.2400732, -53.1805018),
        zoom: 3,
        mapTypeId: 'roadmap'
      });
	  directionsDisplay.setMap(map);
      var infoWindow = new google.maps.InfoWindow;

        downloadUrl("gera-mpcopa.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var type = markers[i].getAttribute("type");
          var point = new google.maps.LatLng(
			  parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));

          var html = "<b>" + name + "</b> <br/>" + address;
          var icon = customIcons[type] || {};
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: icon.icon,
            shadow: icon.shadow
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
		  calcRoute();
		for (var city in citymap) {
    
    var populationOptions = {
      strokeColor: '#0000FF',
      strokeOpacity: 0.23,
      strokeWeight: 100,
      fillColor: '#FF0000',
      fillOpacity: 0.23,
      map: map,
      center: citymap[city].center,
      radius: citymap[city].population / 20 * 2
    };
    cityCircle = new google.maps.Circle(populationOptions);
  }
      });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}
	google.maps.event.addDomListener(window, 'load', initialize);

   </script>

  </head>

  <body onload="load()">
  <div id="panel" align="center">
<strong id="input">Ponto de Saida: </strong>
<select id="start" class="input" onchange="calcRoute();">
		<?php
			$user="F0116002";$pass="gmatos.039";$host="localhost";$banco="mps";
			$con=mysql_connect($host,$user,$pass)or die ("Erro na conex�o com o host.");
			$bd=mysql_select_db($banco, $con)or die("Erro na sele��o do Banco de Dados.");
			$query=mysql_query("SELECT * FROM copa order by id limit 77");
			while($ver=mysql_fetch_array($query)){		
					echo "<option value=\"";echo $ver['latitude'];echo",";echo $ver['longitude'];echo"\">";echo $ver['nome'];echo", ";echo $ver['categoria'];echo"</option>";		
				}
		?>
  </select>
<strong id="input">Ponto de chegada: </strong>
<select id="end"  class="input" onchange="calcRoute();">
		<?php
			$user="F0116002";$pass="gmatos.039";$host="localhost";$banco="mps";
			$con=mysql_connect($host,$user,$pass)or die ("Erro na conex�o com o host.");
			$bd=mysql_select_db($banco, $con)or die("Erro na sele��o do Banco de Dados.");
			$query=mysql_query("SELECT * FROM copa order by id limit 77");
			while($ver=mysql_fetch_array($query)){		
					echo "<option value=\"";echo $ver['latitude'];echo",";echo $ver['longitude'];echo"\">";echo $ver['nome'];echo", ";echo $ver['categoria'];echo"</option>";		
				}
		?>
</select>
</div>
    <div id="map"></div>
  </body>

</html>