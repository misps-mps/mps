<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>jQuery plugin: Tablesorter 2.0 - jQuery UITheme Widget (Bootstrap)</title>

        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="../css/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="../js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<link rel="stylesheet" href="../assets/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/theme.bootstrap.css">
	<script src="../vendors/jquery.tablesorter.js"></script>
	<script src="../vendors/jquery.tablesorter.widgets.js"></script>
	<link rel="stylesheet" href="../vendors/pager/jquery.tablesorter.pager.css">
	<script src="../vendors/pager/jquery.tablesorter.pager.js"></script>

	<script id="js">$(function() {

	$.extend($.tablesorter.themes.bootstrap, {
		// these classes are added to the table. To see other table classes available,
		// look here: http://twitter.github.com/bootstrap/base-css.html#tables
		table      : 'table table-bordered',
		caption    : 'caption',
		header     : 'bootstrap-header', // give the header a gradient background
		footerRow  : '',
		footerCells: '',
		icons      : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
		sortNone   : 'bootstrap-icon-unsorted',
		sortAsc    : 'icon-chevron-up glyphicon glyphicon-chevron-up',     // includes classes for Bootstrap v2 & v3
		sortDesc   : 'icon-chevron-down glyphicon glyphicon-chevron-down', // includes classes for Bootstrap v2 & v3
		active     : '', // applied when column is sorted
		hover      : '', // use custom css here - bootstrap class may not override it
		filterRow  : '', // filter row class
		even       : '', // odd row zebra striping
		odd        : ''  // even row zebra striping
	});

	// call the tablesorter plugin and apply the uitheme widget
	$("table").tablesorter({
		// this will apply the bootstrap theme if "uitheme" widget is included
		// the widgetOptions.uitheme is no longer required to be set
		theme : "bootstrap",

		widthFixed: true,

		headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

		// widget code contained in the jquery.tablesorter.widgets.js file
		// use the zebra stripe widget if you plan on hiding any rows (filter widget)
		widgets : [ "uitheme", "filter", "zebra" ],

		widgetOptions : {
			// using the default zebra striping class name, so it actually isn't included in the theme variable above
			// this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
			zebra : ["even", "odd"],

			// reset filters button
			filter_reset : ".reset"

			// set the uitheme widget to use the bootstrap theme class names
			// this is no longer required, if theme is set
			// ,uitheme : "bootstrap"

		}
	})
	.tablesorterPager({

		// target the pager markup - see the HTML block below
		container: $(".ts-pager"),

		// target the pager page select dropdown - choose a page
		cssGoto  : ".pagenum",

		// remove rows from the table to speed up the sort of large tables.
		// setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
		removeRows: false,

		// output string - default is '{page}/{totalPages}';
		// possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
		output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

	});

});</script>

	<script>
	$(function(){

		// filter button demo code
		$('button.filter').click(function(){
			var col = $(this).data('column'),
				txt = $(this).data('filter');
			$('table').find('.tablesorter-filter').val('').eq(col).val(txt);
			$('table').trigger('search', false);
			return false;
		});

		// toggle zebra widget
		$('button.zebra').click(function(){
			var t = $(this).hasClass('btn-success');
//			if (t) {
				// removing classes applied by the zebra widget
				// you shouldn't ever need to use this code, it is only for this demo
//				$('table').find('tr').removeClass('odd even');
//			}
			$('table')
				.toggleClass('table-striped')[0]
				.config.widgets = (t) ? ["uitheme", "filter"] : ["uitheme", "filter", "zebra"];
			$(this)
				.toggleClass('btn-danger btn-success')
				.find('i')
				.toggleClass('icon-ok icon-remove glyphicon-ok glyphicon-remove').end()
				.find('span')
				.text(t ? 'disabled' : 'enabled');
			$('table').trigger('refreshWidgets', [false]);
			return false;
		});
	});
	</script>

</head>
<body>
<div id="main">

	<div id="demo"><table class="tablesorter">
	<thead>
		<tr>
			<th>Data</th>
			<th>Local</th>
			<th class="filter-select filter-exact" data-placeholder="">Zona</th>
			<th>In&iacutecio</th>
			<th>Fim</th>
			<th class="filter-select filter-exact" data-placeholder="">Situa&ccedil&atildeo</th>
	</thead>
	<tfoot>
		<tr>
			<th>Data</th>
			<th>Local</th>
			<th class="filter-select filter-exact" data-placeholder="">Zona</th>
			<th>In&iacutecio</th>
			<th>Fim</th>
			<th>Situa&ccedil&atildeo</th>
		</tr>
		<tr>
			<th colspan="7" class="ts-pager form-horizontal">
				<button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
				<button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
				<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
				<button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
				<button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
				<select class="pagesize input-mini" title="Select page size">
					<option selected="selected" value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="40">40</option>
				</select>
				<select class="pagenum input-mini" title="Select page number"></select>
			</th>
		</tr>
	</tfoot>
	<tbody>
	<?php
		$user="F0116002";
		$pass="gmatos.039";
		$host="localhost";
		$banco="mps";
		$con=mysql_connect($host,$user,$pass)or die ("Erro na conex�o com o host.");
		$bd=mysql_select_db($banco, $con)or die("Erro na sele��o do Banco de Dados.");
		$query=mysql_query("SELECT * FROM alagamentos");
		while($ver=mysql_fetch_array($query)){
					echo "<tr>";
						echo "<td>"; echo $ver['data']; echo "</td>";
						echo "<td>"; echo $ver['endereco']; echo "</td>";
						echo "<td>"; echo $ver['zona']; echo "</td>";
						echo "<td>"; echo $ver['inicio']; echo "</td>";
						echo "<td>"; echo $ver['fim']; echo "</td>";
						echo "<td>"; echo $ver['situacao']; echo "</td>";
					echo "</tr>";			
		}
	?>
	</tbody>
</table></div>

</div>

</body>
</html>

