<?php 
require("seguranca.php"); // Inclui o arquivo com o sistema de seguran�a
protegePagina(); // Chama a fun��o que protege a p�gina
?>
<!DOCTYPE html>
<html class="no-vendors">
    <head>
        <title>MPS // Monitoramento Preventivo e Sistemas</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
		<link rel="shortcut icon" href="images/favicon.ico">
		<link href="assets/modal.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.vendors"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2.min.js"></script>

    </head>
    
     <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <img src="images/logo-top.png" class="brand"><a class="brand" href="index.php">MPS</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                         
							<li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i><?php echo" "; echo $_SESSION['usuarioNome']?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="0" id="create-user">Alterar Senha</a>
                                    </li>   
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>                               
                                </ul>
                            </li>
							
                        </ul>
							<ul class="nav pull-right">
								<li><a href="#"><i class="icon-tag"></i><?php echo" "; echo $_SESSION['usuarioLogin']?></a></li>	
							</ul>   
                        <ul class="nav">
                            <li class="active">
                                <a href="./">Dashboard</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Alagamentos <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" href="alagamentos.php#mapa-alagamentos">Mapa</a></li>
                                    <li><a tabindex="-1" href="alagamentos.php#form-alagamentos">Cadastrar novo Ponto</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Alertas <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" href="alertas.php#lista-alertas">Listar</a></li>
                                    <li><a tabindex="-1" href="alertas.php#form-alertas">Cadastrar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Clima <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a tabindex="-1" href="clima.php#form-clima">Inserir Informa��es</a></li>
                                    <li><a tabindex="-1" href="clima.php#graficos-clima">Gr�ficos</a></li>
                                </ul>
                            </li>
                            <li><a href="sumario_incidentes.php">Incidentes</a></li>							
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">