<?php

/**
  *   
 */

	class Database{

		function __construct(){
			$this->connect();
		}

		function connect(){
			# connect
			if(!@mysql_connect($this->conf('server'), $this->conf('user'), $this->conf('pass'))){
				$this->critical_error('Não foi possivel conectar no servidor do banco de dados');
			}

			# change db
			if(!@mysql_select_db($this->conf('db'))){
				$this->critical_error('Não foi possivel selecionar o banco de dados');
			}
		}

		function execute($string){
			return mysql_query($string);
		}

		function critical_error($description){
			$html = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<title>Erro Crítico</title>
			</head>
			<body>
				<h1>Erro Crítico</h1>
				<p>Ocorreu um erro grave que impede o correto funcionamento da aplicação.</p>
				<!-- '.$description.' -->
			</body>
			</html>';

			header('Content-Type: text/html; charset=utf-8');
			echo $html;
			exit;
		}
		
		function conf($name){
			$this->conf_exists();
			$option = json_decode(file_get_contents('./conf/database'), true);
			return (empty($option[$name])) ? false : $option[$name] ;
		}

		function conf_exists(){
			# eu sei não é bunito, mas funciona _\,,/
			if(!file_exists('./conf/database')){
				$this->critical_error('O arquivo de configuração do banco de dados não existe');
			}
		}

		function alomaua(){
			echo 'estou aqui';
		}
	}