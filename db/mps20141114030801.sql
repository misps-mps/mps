CREATE DATABASE  IF NOT EXISTS `mps` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `mps`;
-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 127.0.0.1    Database: mps
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aplicacoes`
--

DROP TABLE IF EXISTS `aplicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicacoes`
--

LOCK TABLES `aplicacoes` WRITE;
/*!40000 ALTER TABLE `aplicacoes` DISABLE KEYS */;
INSERT INTO `aplicacoes` VALUES (1,'AVAYA'),(2,'BANCO DE DADOS'),(3,'BANNER'),(4,'BENNER'),(5,'BENNER / RDM 17825'),(6,'BLACKBERRY'),(7,'CCM7'),(8,'CITRIX'),(9,'CMS AVAYA'),(10,'COL'),(11,'CONTROLADORA'),(12,'CONTROLLOCK'),(13,'CRIVO'),(14,'CTI'),(15,'DAF'),(16,'DISCADORA'),(17,'ESEG'),(18,'F5'),(19,'FAX'),(20,'FRONT END'),(21,'GCDE'),(22,'GPRS'),(23,'GPS'),(24,'HOME OFFICE'),(25,'HOME OFFICE - PORTONET'),(26,'HOME OFFICE - VDI'),(27,'HOME OFFICE - VPN'),(28,'INFORMIX'),(29,'INFRA ESTRUTURA'),(30,'INTEGRADOR'),(31,'INTERNET'),(32,'IRIS'),(33,'IXIA'),(34,'IXIA - HOME OFFICE'),(35,'LAN'),(36,'LINHA VERDE'),(37,'LINK DE DADOS'),(38,'LINK DE DADOS/VOZ'),(39,'LINK DE VOZ'),(40,'LINK DE VOZ/DADOS'),(41,'LINK VOZ'),(42,'NANOCOM'),(43,'NAVTEQ'),(44,'NET CLINICA'),(45,'NETCLINICA'),(46,'NETCLINICA / PORTOMED WEB'),(47,'NICE PERFORM'),(48,'NORTEL VPN'),(49,'OMNILINK'),(50,'PABX'),(51,'PAYWARE'),(52,'PCN'),(53,'PORTOMED'),(54,'PORTOMED WEB'),(55,'PPWEB'),(56,'PROTHEUS'),(57,'QUANTUM'),(58,'REDE'),(59,'SAP'),(60,'SAP BO'),(61,'SIACON'),(62,'SIEBEL'),(63,'SIGMA'),(64,'SOFTPHONE'),(65,'SYSIN'),(66,'TELEFONIA'),(67,'URA'),(68,'VDI'),(69,'VDI HOME OFFICE'),(70,'VPN'),(71,'VPN - HOME OFFICE'),(72,'WEB'),(73,'WEB E INFORMIX'),(74,'XGEN'),(75,'XGEN - HOME OFFICE');
/*!40000 ALTER TABLE `aplicacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas_afetadas`
--

DROP TABLE IF EXISTS `areas_afetadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_afetadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_afetadas`
--

LOCK TABLES `areas_afetadas` WRITE;
/*!40000 ALTER TABLE `areas_afetadas` DISABLE KEYS */;
INSERT INTO `areas_afetadas` VALUES (1,'sem operação'),(2,'administração de pagamentos'),(3,'sinistro auto'),(4,'atendimento sinistro'),(5,'aceitação auto'),(6,'aceitacao ramos elementares'),(7,'aceitacao re'),(8,'aceitacao vida'),(9,'adm porto - central atendimento - centro médico'),(10,'administrativo - total'),(11,'alagoas'),(12,'alarmes monitorados'),(13,'almorifado'),(14,'alphaville'),(15,'amazonas'),(16,'araçtuba'),(17,'atendimento e emissao re'),(18,'auditoria medica'),(19,'auto caps'),(20,'azul seguros'),(21,'bahia'),(22,'barra da tijuca'),(23,'bio qualynet'),(24,'blumenau'),(25,'bragança paulista'),(26,'byoqualinet vergueiro'),(27,'cap'),(28,'caps'),(31,'central 24hs'),(32,'central cartao'),(33,'central daf'),(34,'central de cartao'),(35,'central de operacoes '),(37,'central saúde'),(38,'central vida e previdencia'),(39,'central24hs'),(40,'cobrança'),(41,'conecta'),(42,'controle operacoes caps'),(43,'call center - ita?'),(44,'canais eletronicos'),(45,'canal medico'),(46,'cap'),(47,'cap - central de atendimento ao prestador'),(48,'cap saude'),(49,'caps'),(50,'caps vistoria previa'),(51,'car belo horizonte'),(53,'cartao'),(54,'cartao clientes'),(55,'cartao corretores'),(56,'cartao cobranca'),(57,'cartao cliente'),(59,'cartão analise de risco/corretores/processos/contestação'),(60,'cascavel'),(61,'ceara'),(62,'centra vida prev'),(63,'central cobranca'),(64,'central daf'),(65,'central de agendamento daf'),(66,'central de localizacao'),(67,'central de operacoes'),(68,'central de operacoes - psg'),(71,'central de servi?os'),(72,'central de servi?os info'),(73,'central operacaes'),(74,'central operacoes'),(75,'central pat'),(76,'central pat saude'),(77,'central saude'),(78,'central sinistro'),(79,'central vida e previd?ncia'),(80,'central vida e previdencia'),(81,'central vida prev'),(82,'chapeco'),(83,'chat citrino -'),(84,'cobrana?a'),(85,'cobranca'),(86,'cobranca / cartao'),(87,'cobranca atd'),(88,'cobranca operacional'),(89,'comercial'),(90,'comiss?es'),(91,'compras'),(92,'conecta'),(93,'cons?rcio'),(94,'cons?rcio - belo horizonte'),(95,'contas a pagar'),(96,'contas a receber'),(97,'controladoria'),(98,'controle de opera??es'),(99,'controle de produ??o'),(100,'criciuma'),(101,'ct24hs'),(102,'diadema'),(103,'divin?polis'),(104,'emissao re'),(105,'emissao saude'),(106,'emissao vida'),(107,'emissao vida empresarial'),(108,'emissão aluguel'),(109,'emissão auto'),(110,'emissão re'),(111,'emissão saude'),(112,'emissa aluguel'),(113,'emissa?o auto e na?cleo de nega?cios'),(114,'emissao'),(115,'emissao - web'),(116,'emissao aluguel'),(117,'emissao atend ramos elementares'),(118,'emissao auto'),(119,'emissao automovel'),(120,'emissao de ramos elementares'),(121,'emissao re'),(122,'emissao re corretores e segurados'),(123,'emissao saude'),(124,'emissao transportes'),(125,'emissao vida'),(126,'emissao vida e previd?ncia'),(127,'emissao vida e previdencia'),(128,'emissao vida empresarial'),(129,'emissao vida individual'),(130,'emissao vida inidividual'),(131,'espa?o carr?o'),(132,'espa?o de neg carr?o'),(133,'espa?o de neg formosa'),(134,'espa?o de neg?cio carr?o'),(135,'espa?o de negocios formosa'),(136,'espa?o formosa'),(137,'espa?o santana'),(138,'espaco de negocios formosa'),(139,'espirito santo'),(140,'financeiro'),(141,'financeiro'),(142,'financeiro -'),(143,'franca'),(144,'gest?o de contas e proj'),(145,'gest?o de riscos'),(146,'gest?o saude'),(147,'goc'),(148,'goc aluguel'),(149,'gprs nanocom indisponivel'),(150,'help desk corretores'),(151,'home office'),(152,'home office e plantonistas'),(153,'home office '),(154,'hd condolidado'),(155,'hd consolidado'),(156,'hd corretores'),(157,'hdc'),(158,'hdcasa'),(159,'help desk casa'),(160,'help desk casa + corretores'),(161,'help desk corretores'),(162,'helpdesk corretores'),(163,'home office'),(164,'home office atendimento'),(165,'home office sinistro'),(166,'imbiribeira'),(167,'imobiliario'),(168,'indaiatuba'),(169,'integra??o de sistemas'),(170,'interlagos'),(171,'itatiba'),(172,'joinville'),(173,'juiz de fora'),(174,'largo do café'),(175,'licitações'),(176,'lapa'),(177,'lauro de freitas'),(178,'liberacao pr?via'),(179,'licita??es'),(180,'limeira'),(181,'link de dados indisponivel'),(182,'link de voz e dados indisponiveis'),(183,'link de voz indisponivel'),(184,'litoral paulista'),(185,'londrina'),(186,'marilia'),(187,'maring?'),(188,'marketing'),(189,'marketing vendas online e pat telemarketing'),(190,'minas gerais'),(191,'mogi das cruzes'),(192,'monitoramento saude'),(193,'mooca'),(194,'morumbi'),(195,'n?cleo a preven??o a fraude / sinistro de autos/ramos elemen'),(196,'n?cleo de atendimento'),(197,'n?cleo de negocios'),(198,'nucleo de atendimento'),(199,'niteroi'),(200,'nove de julho'),(201,'npc'),(202,'nucleo atendimento auto'),(203,'nucleo de negocio auto'),(204,'nucleo de negocios'),(205,'nucleo de negocios auto'),(206,'nucleo de negocios/hd/vp'),(207,'nucleo de planejamento e controle'),(208,'nucleo planejamento controle'),(209,'nucleos de negocios'),(210,'núcleo de negócios'),(211,'núcleo de qualidade'),(212,'odontológico'),(213,'opera??es da azul'),(214,'opera??es da tivit'),(215,'operacao cartao'),(216,'operacional'),(217,'or?amento'),(218,'osasco'),(219,'porto aluguel'),(220,'porto atendimento'),(221,'porto conecta'),(222,'portomed'),(223,'processo de emissao'),(224,'processo de emissão auto'),(225,'processo de emissão auto / help desk corretores'),(226,'proteção e monitoramento'),(227,'proteção e monitoramento cliente'),(228,'pabx n?o realza liga??es'),(229,'para'),(230,'paran?'),(231,'parana'),(232,'pat'),(233,'pat auto'),(234,'pat auto /ita?/azul'),(235,'pat azul'),(236,'pat ita'),(237,'pat porto'),(238,'pat porto, azul e ita'),(239,'pat porto/azul/ita'),(240,'pat porto_azul_ita'),(241,'pat saude'),(242,'pat servi?os emergenciais'),(243,'pat servi?os porto'),(244,'pat telemarketing'),(245,'pe auto'),(246,'pernambuco'),(247,'pesquisa e desenv'),(248,'petropolis'),(249,'pinto bandeira'),(250,'piracicaba'),(251,'pirituba'),(252,'planejamento e controle'),(253,'po?os de caldas'),(254,'porto aluguel'),(255,'porto aluguel web'),(256,'porto atendimento'),(257,'porto atendimento telemarketing'),(259,'porto cartao'),(260,'porto par'),(261,'porto saude'),(262,'porto socorro'),(263,'portocap'),(264,'portomed'),(265,'portomed s?o miguel'),(266,'portomed sao miguel'),(267,'portonet indisponivel'),(268,'portopar'),(269,'portoseg'),(270,'possessos de emissao re?'),(271,'pouso alegre'),(272,'pra?a da arvore'),(273,'praca da arvore'),(274,'previdencia'),(275,'processo de emissao'),(276,'processo de emissao - re'),(277,'processo de emissao re'),(278,'processo de emissao vida'),(279,'processo emissao'),(280,'processo emissao auto'),(281,'processos de emissa?o re'),(282,'processos de emissa?o vida'),(283,'processos de emissao'),(284,'processos de emissao vida e previdencia'),(285,'processos emissao'),(286,'processos emissao - re'),(287,'processos emissao auto'),(288,'processos emissao re'),(289,'processos projetos corporativos'),(290,'produ??o / licita??es'),(291,'prot. monit. aceita??o'),(292,'prote??o e monitoramento'),(293,'protecao e monitoramento'),(294,'protecao monitoramento'),(295,'proteção e monitoramento'),(296,'protocolo eletronico'),(297,'psg ct azul sinistro'),(298,'psg ct esp. de sinistro'),(299,'quantum financeiro'),(300,'qualidade e desenvolvimeto'),(301,'ramos elementares'),(302,'risco financeiro'),(303,'ramos elementares'),(304,'ramos elemtares'),(305,'rebou?as'),(306,'recursos humanos'),(307,'reg anhanguera'),(308,'reg barra da tijuca'),(309,'reg blumenal'),(310,'reg cascav?l'),(311,'reg chapec'),(312,'reg criciuma'),(313,'reg feira de santana'),(314,'reg ipiranga'),(315,'reg itatiba'),(316,'reg jacar?pagua'),(317,'reg mogi das cruzes'),(318,'reg mogi mirin'),(319,'reg nova igua?u'),(320,'reg nova iguacu'),(321,'reg penha'),(322,'reg petr?polis'),(323,'reg pinto bandeira'),(324,'reg pouso alegre'),(325,'reg s?o bernardo'),(326,'reg são joão'),(327,'reg são joão da boa vista'),(328,'reg sete lagoas'),(329,'reg sete lagos'),(330,'reg taubat'),(331,'reg turiassu'),(332,'reg uberl?ndia'),(333,'reg vale dos sinos'),(334,'reg vila prudente'),(335,'reg vit?ria da conquista'),(336,'reg vitoria'),(337,'regional aracatuba'),(338,'regional blumenau'),(339,'regional campo grande'),(340,'regional caxias do sul'),(341,'regional chapeco'),(342,'regional chapeco sc'),(343,'regional guaruja'),(344,'regional jaguara'),(345,'regional joinville'),(346,'regional jundiai'),(347,'regional londrina'),(348,'regional marilia'),(349,'regional mogi mirim'),(350,'regional niteroi'),(351,'regional nova igua?u'),(352,'regional petropolis'),(353,'regional pouso alegre'),(354,'regional rondonopolis'),(355,'regional sjrp-sp'),(356,'regional sorocaba'),(357,'regional tijuca'),(358,'regional uberlandia'),(359,'regulacao sinistro'),(360,'rej tijuca'),(361,'ribeir?o preto'),(362,'rio de janeiro'),(363,'rio grande do norte'),(364,'rondonia'),(365,'s?o bernardo do campo'),(366,'s?o caetano do sul'),(367,'s?o miguel'),(368,'saúde'),(369,'sinistro auto'),(370,'sinistro re'),(371,'sinistro re e aluguel'),(372,'sisnistro re'),(373,'sucursais'),(374,'sucursal pernambuco'),(375,'santa catarina'),(376,'saps'),(377,'saude'),(378,'saude - odontol?gico'),(379,'saude cap'),(380,'saude gest?o de prestadores'),(381,'seguranca corporativa'),(382,'servi?os avulsos'),(383,'sete lagoas'),(384,'sinistro'),(385,'sinistro auto'),(386,'sinistro pat'),(387,'sinistro re'),(388,'sites largo do caf'),(389,'sorocaba'),(390,'suc alphaville'),(391,'suc amazonas'),(392,'suc americana'),(393,'suc bauru'),(394,'suc belem'),(395,'suc belo horizonte'),(396,'suc blumenau'),(397,'suc braganca paulista'),(398,'suc campo grande'),(399,'suc campos el?seos'),(400,'suc diadema'),(401,'suc guarulhos'),(402,'suc manaus'),(403,'suc maranh?o'),(404,'suc maranhao'),(405,'suc mogi das cruzes'),(406,'suc natal'),(407,'suc para'),(408,'suc penha - rj'),(409,'suc piau'),(410,'suc porto alegre'),(411,'suc porto velho'),(412,'suc rio de janeiro'),(413,'suc rondonopolis'),(414,'suc s?o bernardo'),(415,'suc s?o caetano'),(416,'suc s?o jose do rio preto'),(417,'suc santa catarina'),(418,'suc santana'),(419,'suc santo amaro'),(420,'suc santos'),(421,'suc sao bernardo do campo'),(422,'suc sao caetano'),(423,'suc sao caetano do sul'),(424,'suc tatuape'),(425,'suc vale do para?ba - sp'),(426,'suc vitoria'),(427,'suc. morumbi'),(428,'suc. porto alegre'),(429,'sucursa brag. paulista'),(430,'sucursal belo horizonte'),(431,'sucursal bh'),(432,'sucursal campinas'),(433,'sucursal campo grande'),(434,'sucursal cascavel'),(435,'sucursal centro sp'),(436,'sucursal criciuma'),(437,'sucursal fortaleza'),(438,'sucursal goiania'),(439,'sucursal indianapolis'),(440,'sucursal jacarepagua'),(441,'sucursal manaus'),(442,'sucursal mooca'),(443,'sucursal natal'),(444,'sucursal pca da arvore'),(445,'sucursal petropolis'),(446,'sucursal piaui'),(447,'sucursal porto alegre'),(448,'sucursal pouso alegre'),(449,'sucursal rebou?as'),(450,'sucursal rio de janeiro'),(451,'sucursal rondonia'),(452,'sucursal s?o bernardo'),(453,'sucursal s?o caetano'),(454,'sucursal salvador'),(455,'sucursal santana - sp'),(456,'sucursal sta catarina'),(457,'sucursal teresina'),(458,'sucursal uberlandia'),(459,'transporte monitorados'),(460,'tatuap'),(461,'tatuape'),(462,'tecnica patrimonial'),(463,'todas'),(464,'todas as ?reas afetadas'),(465,'todas opera??es'),(466,'todos'),(467,'torre de monitoramento'),(468,'transporte e monitoramento'),(469,'transportes monitorador'),(470,'transportes monitorados'),(471,'transpotes monitorados'),(472,'tributos'),(473,'unidade de emissao'),(474,'unidade de emissao vida individual'),(475,'unidades de emissao'),(476,'vida'),(477,'vistoria pr?via'),(478,'vistoria prévia'),(479,'vale dos sinos'),(480,'vida'),(481,'vida e prev'),(482,'vida e previd?ncia'),(483,'vida e previdencia'),(484,'vida emissao coletivo'),(485,'vida emissao individual'),(486,'vida emissao indvidual'),(487,'vida empresarial'),(488,'vida individual'),(489,'vila prudente'),(490,'vistoria pr?via'),(491,'vistoria previa'),(492,'vitoria da conquista'),(493,'xgen');
/*!40000 ALTER TABLE `areas_afetadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_aplicacoes_afetadas`
--

DROP TABLE IF EXISTS `inc_aplicacoes_afetadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_aplicacoes_afetadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incidente` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `aplicacao` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_aplicacoes_afetadas`
--

LOCK TABLES `inc_aplicacoes_afetadas` WRITE;
/*!40000 ALTER TABLE `inc_aplicacoes_afetadas` DISABLE KEYS */;
/*!40000 ALTER TABLE `inc_aplicacoes_afetadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_categorias`
--

DROP TABLE IF EXISTS `inc_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_categorias` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `raiz` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_categorias`
--

LOCK TABLES `inc_categorias` WRITE;
/*!40000 ALTER TABLE `inc_categorias` DISABLE KEYS */;
INSERT INTO `inc_categorias` VALUES (1,'Concessao de Acesso e Servicos TI',0),(2,'Continuidade ao Negocio',0),(3,'Home Office',0),(4,'Projeto Individualizacao de Acesso',0),(5,'Servico Azul Seguros',0),(6,'Servico Corporativo',0),(7,'Servico Data Center',0),(8,'Servico de Dados',0),(9,'Servico de Desktop',0),(10,'Servico de E-mail',0),(11,'Servico de Hardware',0),(12,'Servico de Impressao',0),(13,'Servico de Monitoramento',0),(14,'Servico de Negocio',0),(15,'Servico de Qualidade de Dados',0),(16,'Servico de URA',0),(17,'Servico de Voz',0),(18,'Servico de Voz e Dados',0),(19,'Servico Gestao WFM',0),(20,'Servico Hardware',0),(21,'Servico Home Office',0),(22,'Servico Implantacao CA',0),(23,'Servico PortoSDM',0),(24,'Servico Seguranca da Informacao',0),(25,'Servico Software',0),(26,'Servicos Bioqualynet',0),(27,'Servicos de RH',0),(28,'Servicos Portoseg',0),(29,'Software',0),(30,'Telefonia e Mobilidade',0),(32,'Suporte Emergencial',2);
/*!40000 ALTER TABLE `inc_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_deptos_afetados`
--

DROP TABLE IF EXISTS `inc_deptos_afetados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_deptos_afetados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incidente` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `aplicacao` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_deptos_afetados`
--

LOCK TABLES `inc_deptos_afetados` WRITE;
/*!40000 ALTER TABLE `inc_deptos_afetados` DISABLE KEYS */;
/*!40000 ALTER TABLE `inc_deptos_afetados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_incidentes`
--

DROP TABLE IF EXISTS `inc_incidentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_incidentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `numero` int(15) NOT NULL,
  `severidade` int(1) NOT NULL,
  `categoria` int(11) NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `data_inicio` date DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `data_fim` date DEFAULT NULL,
  `hora_fim` time DEFAULT NULL,
  `insert_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `numero_chamado` (`numero`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_incidentes`
--

LOCK TABLES `inc_incidentes` WRITE;
/*!40000 ALTER TABLE `inc_incidentes` DISABLE KEYS */;
INSERT INTO `inc_incidentes` VALUES (1,'asdadasdas',1,123456,1,2,'adadasdas','2014-11-14',NULL,'2014-11-14',NULL,'2014-11-14 01:58:58'),(2,'asdasdasdasd',1,21312312,1,1,'asdasdas','2014-11-14',NULL,'2014-11-14',NULL,'2014-11-14 02:00:03');
/*!40000 ALTER TABLE `inc_incidentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_status`
--

DROP TABLE IF EXISTS `inc_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_status`
--

LOCK TABLES `inc_status` WRITE;
/*!40000 ALTER TABLE `inc_status` DISABLE KEYS */;
INSERT INTO `inc_status` VALUES (1,'Aberto'),(2,'Aguardando Fornecedores'),(3,'Aguardando Informacao do Cliente'),(4,'Aguardando RDM'),(5,'Concluido'),(6,'Em Atendimento'),(7,'Em Homologacao Pelo Cliente'),(8,'Encerrado');
/*!40000 ALTER TABLE `inc_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-14  3:09:40
