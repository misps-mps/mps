CREATE DATABASE  IF NOT EXISTS `mps` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `mps`;
-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 127.0.0.1    Database: mps
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aplicacoes`
--

DROP TABLE IF EXISTS `aplicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicacoes`
--

LOCK TABLES `aplicacoes` WRITE;
/*!40000 ALTER TABLE `aplicacoes` DISABLE KEYS */;
INSERT INTO `aplicacoes` VALUES (1,'AVAYA'),(2,'BANCO DE DADOS'),(3,'BANNER'),(4,'BENNER'),(5,'BENNER / RDM 17825'),(6,'BLACKBERRY'),(7,'CCM7'),(8,'CITRIX'),(9,'CMS AVAYA'),(10,'COL'),(11,'CONTROLADORA'),(12,'CONTROLLOCK'),(13,'CRIVO'),(14,'CTI'),(15,'DAF'),(16,'DISCADORA'),(17,'ESEG'),(18,'F5'),(19,'FAX'),(20,'FRONT END'),(21,'GCDE'),(22,'GPRS'),(23,'GPS'),(24,'HOME OFFICE'),(25,'HOME OFFICE - PORTONET'),(26,'HOME OFFICE - VDI'),(27,'HOME OFFICE - VPN'),(28,'INFORMIX'),(29,'INFRA ESTRUTURA'),(30,'INTEGRADOR'),(31,'INTERNET'),(32,'IRIS'),(33,'IXIA'),(34,'IXIA - HOME OFFICE'),(35,'LAN'),(36,'LINHA VERDE'),(37,'LINK DE DADOS'),(38,'LINK DE DADOS/VOZ'),(39,'LINK DE VOZ'),(40,'LINK DE VOZ/DADOS'),(41,'LINK VOZ'),(42,'NANOCOM'),(43,'NAVTEQ'),(44,'NET CLINICA'),(45,'NETCLINICA'),(46,'NETCLINICA / PORTOMED WEB'),(47,'NICE PERFORM'),(48,'NORTEL VPN'),(49,'OMNILINK'),(50,'PABX'),(51,'PAYWARE'),(52,'PCN'),(53,'PORTOMED'),(54,'PORTOMED WEB'),(55,'PPWEB'),(56,'PROTHEUS'),(57,'QUANTUM'),(58,'REDE'),(59,'SAP'),(60,'SAP BO'),(61,'SIACON'),(62,'SIEBEL'),(63,'SIGMA'),(64,'SOFTPHONE'),(65,'SYSIN'),(66,'TELEFONIA'),(67,'URA'),(68,'VDI'),(69,'VDI HOME OFFICE'),(70,'VPN'),(71,'VPN - HOME OFFICE'),(72,'WEB'),(73,'WEB E INFORMIX'),(74,'XGEN'),(75,'XGEN - HOME OFFICE');
/*!40000 ALTER TABLE `aplicacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aplicacoes_area`
--

DROP TABLE IF EXISTS `aplicacoes_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicacoes_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` int(11) DEFAULT NULL,
  `aplicacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicacoes_area`
--

LOCK TABLES `aplicacoes_area` WRITE;
/*!40000 ALTER TABLE `aplicacoes_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `aplicacoes_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` VALUES (1,'CENTRAL DE CARTAO'),(2,'CENTRAL SAUDE'),(3,'SUCURSAIS'),(4,'PORTOMED');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_aplicacoes_afetadas`
--

DROP TABLE IF EXISTS `inc_aplicacoes_afetadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_aplicacoes_afetadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incidente` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `aplicacao` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_aplicacoes_afetadas`
--

LOCK TABLES `inc_aplicacoes_afetadas` WRITE;
/*!40000 ALTER TABLE `inc_aplicacoes_afetadas` DISABLE KEYS */;
INSERT INTO `inc_aplicacoes_afetadas` VALUES (12,'a2405a91597544aa3fc948eb16a73bd2',51),(13,'ebadb91054c10584da3ee79ee0810cbc',51),(14,'cdbb0bd06df73f6534852f71262fb179',51);
/*!40000 ALTER TABLE `inc_aplicacoes_afetadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_areas_afetadas`
--

DROP TABLE IF EXISTS `inc_areas_afetadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_areas_afetadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incidente` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `area` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_areas_afetadas`
--

LOCK TABLES `inc_areas_afetadas` WRITE;
/*!40000 ALTER TABLE `inc_areas_afetadas` DISABLE KEYS */;
INSERT INTO `inc_areas_afetadas` VALUES (21,'a2405a91597544aa3fc948eb16a73bd2',1),(22,'a2405a91597544aa3fc948eb16a73bd2',2),(23,'a2405a91597544aa3fc948eb16a73bd2',3),(24,'ebadb91054c10584da3ee79ee0810cbc',3),(25,'cdbb0bd06df73f6534852f71262fb179',2);
/*!40000 ALTER TABLE `inc_areas_afetadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_categorias`
--

DROP TABLE IF EXISTS `inc_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_categorias` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `raiz` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_categorias`
--

LOCK TABLES `inc_categorias` WRITE;
/*!40000 ALTER TABLE `inc_categorias` DISABLE KEYS */;
INSERT INTO `inc_categorias` VALUES (1,'Concessao de Acesso e Servicos TI',0),(2,'Continuidade ao Negocio',0),(3,'Home Office',0),(4,'Projeto Individualizacao de Acesso',0),(5,'Servico Azul Seguros',0),(6,'Servico Corporativo',0),(7,'Servico Data Center',0),(8,'Servico de Dados',0),(9,'Servico de Desktop',0),(10,'Servico de E-mail',0),(11,'Servico de Hardware',0),(12,'Servico de Impressao',0),(13,'Servico de Monitoramento',0),(14,'Servico de Negocio',0),(15,'Servico de Qualidade de Dados',0),(16,'Servico de URA',0),(17,'Servico de Voz',0),(18,'Servico de Voz e Dados',0),(19,'Servico Gestao WFM',0),(20,'Servico Hardware',0),(21,'Servico Home Office',0),(22,'Servico Implantacao CA',0),(23,'Servico PortoSDM',0),(24,'Servico Seguranca da Informacao',0),(25,'Servico Software',0),(26,'Servicos Bioqualynet',0),(27,'Servicos de RH',0),(28,'Servicos Portoseg',0),(29,'Software',0),(30,'Telefonia e Mobilidade',0),(32,'Suporte Emergencial',2);
/*!40000 ALTER TABLE `inc_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_incidentes`
--

DROP TABLE IF EXISTS `inc_incidentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_incidentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `numero` int(15) NOT NULL,
  `severidade` int(1) NOT NULL,
  `categoria` int(11) DEFAULT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `inicio` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `numero_chamado` (`numero`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_incidentes`
--

LOCK TABLES `inc_incidentes` WRITE;
/*!40000 ALTER TABLE `inc_incidentes` DISABLE KEYS */;
INSERT INTO `inc_incidentes` VALUES (22,'a2405a91597544aa3fc948eb16a73bd2',9,123456789,1,0,'Titulo: Incidente de Testes','Descricao: Incidente de Testes','2014-10-01 00:00:00','2014-10-01 00:00:00','2014-11-19 01:15:23'),(23,'ebadb91054c10584da3ee79ee0810cbc',9,12345678,1,0,'Titulo: Incidente de Testes','Descricao: Incidente de Testes','2014-10-01 00:00:00','2014-10-01 00:00:00','2014-11-19 01:15:23'),(24,'cdbb0bd06df73f6534852f71262fb179',9,1234567,1,0,'Titulo: Incidente de Testes','Descricao: Incidente de Testes','2014-10-01 00:00:00','2014-10-01 00:00:00','2014-11-19 01:15:23');
/*!40000 ALTER TABLE `inc_incidentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inc_status`
--

DROP TABLE IF EXISTS `inc_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inc_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inc_status`
--

LOCK TABLES `inc_status` WRITE;
/*!40000 ALTER TABLE `inc_status` DISABLE KEYS */;
INSERT INTO `inc_status` VALUES (1,'Aberto'),(2,'Aguardando Fornecedores'),(3,'Aguardando Informacao do Cliente'),(4,'Aguardando RDM'),(5,'Concluido'),(6,'Em Atendimento'),(7,'Em Homologacao Pelo Cliente'),(8,'Encerrado'),(9,'Fechado');
/*!40000 ALTER TABLE `inc_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-19  3:23:54
