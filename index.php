<?php include('header.php'); ?>
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Home</li>
	                                </ul>
                            	</div>
                        	</div>
                    </div>
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Apresenta��o</div>
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="block-content collapse in">
								<img src="images/fundo.png"><br><br>
								<p style="line-height:150%; text-align: justify; text-indent:50px;">Como Plano Estrat�gico, o MPS, tem por objetivo realizar a an�lise preventiva de URA, greves, paralisa��es, eventos e sitemas. Atuar na escala��o dos chamados de TI e na an�lise e aprova��o de RDM de tecnologia (sistemas, telecom, etc.). Tamb�m, participar no prcesso de PCN e an�lise sobre a recorr�ncia de determinados problemas.</p>
								
								<p style="line-height:150%; text-align: justify; text-indent:50px;">Em rela��o ao processo operacional, atuamos no envio de boletins sobre alertas preventivos, em abertura de chamados (para incidentes identificados previamete), com monitoramento e an�lise dos poss�veis impactos de TI nas centrais de atendimento e organiza��o e acompanhamento dos seus respectivos movimentos programados.</p>
								
								<p style="line-height:150%; text-align: justify; text-indent:50px;">Neste portal voc� encontra informa��es sobre previs�o do tempo e pluviom�trica, mapa com pontos de alagamentos j� registrados em S�o Paulo, alertas de novas greves e paralisa��es, mapa das bases dos prestadores, funcion�rios e pontos de encontro, sum�rio de todos os incidentes registrados pela companhia e outras informa��es. Bem Vindos!</p>
								
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block span6">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Transito</div>
                                <div class="pull-right"><a href="transito.php"><span class="badge badge-warning">Ver Mais</span></a>
					
                                </div>
                            </div>
                            <div class="block-content collapse in">
	<center><iframe scrolling="no" src="frames/frame-sptrans.php" width="100%" height="255px" frameborder="0"></iframe></center>
                            </div>
                        </div>
                        <!-- /block -->
                        <!-- block -->
                        <div class="block span6">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Clima</div>
                                <div class="pull-right"><a href="clima.php"><span class="badge badge-warning">Ver Mais</span></a>
					
                                </div>
                            </div>
                            <div class="block-content collapse in">
								<center><iframe scrolling="no" src="frames/frame-clima.php" width="100%" height="255px" frameborder="0"></iframe></center>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
					<div class="row-fluid">
							<!-- block -->
							<div class="block span4">
								<div class="navbar navbar-inner block-header">
									<div class="muted pull-left">CPTM // Metr�</div>
								</div>
								<div class="block-content collapse in" style="background-color: #f6f6f6;">
											<center>
												<table style="background-color: #f6f6f6;">
													<tr style="vertical-align: top;" width="320px" color="#f6f6f6"><iframe scrolling="no" src="frames/frame-cptm.php" width="110%" height="220px" frameborder="0"></iframe></tr>
													<tr style="text-align:center; "><iframe scrolling="no" src="frames/frame-metro.php" width="100%" height="270px" frameborder="0"></iframe></tr>
												</table>
											</center>
								</div>
							</div>
							<!-- /block -->
							<!-- block -->
							<div class="block span8">
								<div class="navbar navbar-inner block-header">
									<div class="muted pull-left">Manisfesta��es</div>
								</div>
								<div class="block-content collapse in">
									<center><iframe scrolling="yes" src="frames/frame-alertas.php" width="100%" height="495px" frameborder="0"></iframe></center>
								</div>
							</div>
							<!-- /block -->						
					</div>
				</div>
<?php include('footer.php'); ?>

				
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>		
  <script>
  $(function() {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      name = $( "#name" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );

    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser() {
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      valid = valid && checkLength( name, "username", 3, 16 );
      valid = valid && checkLength( email, "email", 6, 80 );
      valid = valid && checkLength( password, "password", 5, 16 );
 
      valid = valid && checkRegexp( name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
      valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
      valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
 
      if ( valid ) {
        $( "#users tbody" ).append( "<tr>" +
          "<td>" + name.val() + "</td>" +
          "<td>" + email.val() + "</td>" +
          "<td>" + password.val() + "</td>" +
        "</tr>" );
        dialog.dialog( "close" );
      }
      return valid;
    }
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 300,
      width: 350,
      modal: true,
      buttons: {
        "Create an account": addUser,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser();
    });
 
    $( "#create-user" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });
  });
  </script>
  <div id="dialog-form" title="Create new user">
  <p class="validateTips">Todos os campos s�o obrigat�rios</p>
 
  <form method="post" action="inserts/alterar-senha.php">
    <fieldset>
      <label for="name">Login</label>
      <input type="text" name="login" id="name" value="" class="text ui-widget-content ui-corner-all">
      <label for="password">Senha</label>
      <input type="password" name="senha" id="password" value="xxxxxxx" class="text ui-widget-content ui-corner-all">
 
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
<!-- QUEMSOU: <?php echo $_SERVER['SERVER_ADDR'];?> -->