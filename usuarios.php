<?php include ('header.php');
echo"<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js\"></script>";
include ('table.php');
?>      
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--/span-->
<div class="span9" id="content">			 
	<div class="row-fluid">
		 <!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">Adicionar Usuários</div>
			</div>
			<div class="block-content collapse in">
				<div class="span12">
					<div id="rootwizard">
						<div class="navbar">
						  <div class="navbar-inner">
							<div class="container">
						<ul>
							<li><a href="#tab1" data-toggle="tab">Dados</a></li>
							<li><a href="#tab2" data-toggle="tab">Usu&aacuterios</a></li>
							<li><a href="#tab3" data-toggle="tab">Privil&eacutegios</a></li>
						</ul>
						 </div>
						  </div>
						</div>
						<div id="bar" class="progress progress-striped active">
						  <div class="bar"></div>
						</div>
						<div class="tab-content">
							<div class="tab-pane" id="tab1">
							   <form class="form-horizontal" id="form1">
								  <fieldset>
									<div class="control-group">
									  <label class="control-label" for="focusedInput">Nome</label>
									  <div class="controls">
										<input class="input-xlarge focused" type="text" name="nome" id="nome">
									  </div>
									</div>
									<div class="control-group">
									  <label class="control-label" for="focusedInput">Matr&iacutecula</label>
									  <div class="controls">
										<input class="input-xlarge focused" id="email" type="text" name="email">
									  </div>
									</div>
								  </fieldset>
								</form>
							</div>
							<div class="tab-pane" id="tab2">
								<form class="form-horizontal" id="form2">
								  <fieldset>
									<div class="control-group">
									  <label class="control-label" for="focusedInput">Usu&aacuterio de Login</label>
									  <div class="controls">
										<input class="input-xlarge focused" id="usuario" type="text" name="usuario">
									  </div>
									</div>
									<div class="control-group">
									  <label class="control-label" for="focusedInput">Senha</label>
									  <div class="controls">
										<input class="input-xlarge focused" id="senha" type="text" name="senha">
									  </div>
									</div>
								  </fieldset>
								</form>
							</div>
							<div class="tab-pane" id="tab3">
								<form class="form-horizontal" id="form3">
								  <fieldset>
									<div class="control-group">
									  <label class="control-label" for="focusedInput">Categoria de Usu&aacuterio</label>
									  <div class="controls">
										<select class="input-xlarge focused" id="categoria" type="text" name="categoria">
											<option value="admin">Admnistrador</option>
											<option value="comum">Comum</option>
										</select>
									  </div>
									</div>
									<div class="control-group">
									  <label class="control-label" for="focusedInput">Pode ver</label>
									  <div class="controls">
										<select class="input-xlarge focused" id="privilegio" type="text" name="privilegio">
											<option value="tudo">Tudo</option>
											<option value="4">Clima</option>
											<option value="4">Pontos de Alagamento</option>
											<option value="4">Alertas</option>
											<option value="4">Copa</option>
											<option value="4">A&ccedil&otildees para Greve</option>
											<option value="4">Usu&aacuterios</option>
											<option value="4">Sum&aacuterio de Incidentes</option>
											<option value="4">Incidentes 2 e 3</option>
											<option value="4">Boletim Informativo</option>
											<option value="4">Plant&atildeo Torre</option>
										</select>
									  </div>
									</div>												
								  </fieldset>
								</form>
							</div>											
							<ul class="pager wizard">
								<li class="previous first" style="display:none;"><a href="javascript:void(0);">Primeiro</a></li>
								<li class="previous"><a href="javascript:void(0);">Anterior</a></li>
								<li class="next last" style="display:none;"><a href="javascript:void(0);">&Uacuteltimo</a></li>
								<li class="next"><a href="javascript:void(0);">Pr&oacuteximo</a></li>
								<li class="next finish" style="display:none;"><a href="javascript:;">Finalizar</a></li>
							</ul>
						</div>  
					</div>
				</div>
			</div>
		</div>
		<!-- /block -->
	</div>
<!-- /wizard -->
	<div class="row-fluid">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">Relação de Usuários</div>
				<div class="pull-right"></div>
			</div>
			<div class="block-content collapse in">
				<div id="demo">
					<table class="tablesorter" style="width:100%">
					<thead>
						<tr>
							<th><center>Nome</center></th>
							<th class="filter-select filter-exact"><center>Categoria</center></th>
							<th><center>Pode ver</center></th>
							<th><center>Usu&aacuterio</center></th>
							<th><center>Senha</center></th>
							<th class="filter-select filter-exact"center><center>A&ccedil&otildees</center></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th><center>Nome</center></th>
							<th class="filter-select filter-exact"><center>Categoria</center></th>
							<th><center>Pode ver</center></th>
							<th><center>Usu&aacuterio</center></th>
							<th><center>Senha</center></th>
							<th class="filter-select filter-exact"center><center>A&ccedil&otildees</center></th>
						</tr>
						<tr>
							<th colspan="7" class="ts-pager form-horizontal">
							<button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
							<button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
							<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
							<button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
							<button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
							<select class="pagesize input-mini" title="Select page size">
								<option selected="selected" value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
							</select>
							<select class="pagenum input-mini" title="Select page number"></select>
							</th>
						</tr>
					</tfoot>
					<?php
					echo"<tbody>";
					$query=mysql_query("SELECT * FROM usuarios");
					while($ver=mysql_fetch_array($query)){
						if($ver['categoria'] == '1'){
							$cat = 'Adm. Geral';
						}else{
							$cat = 'Usu&aacuterio Comum';
						}
						if($ver['privilegio'] == '0'){
							$pri = 'Tudo';
						}elseif($ver['privilegio'] == '2'){
							$pri = 'Tesouraria';
						}elseif($ver['privilegio'] == '3'){
							$pri = 'Tudo';
						}										
						
						echo "<tr>";
							echo "<td><center>"; echo $ver['nome'];echo" "; echo $ver['sobrenome'];echo "</center></td>";
							echo "<td><center>"; echo $cat; echo "</center></td>";
							echo "<td><center>"; echo $pri; echo "</center></td>";
							echo "<td><center>"; echo $ver['usuario']; echo "</center></td>";
							echo "<td><center>"; echo $ver['senha']; echo "</center></td>";
							echo "<td><center><form name=\"id\" method=\"post\" action=\"inserts/alter_user.php\"><input name=\"id_del\" type=\"hidden\" value=\"";echo$ver['id'];echo"\">
							<button class=\"btn btn-danger btn-mini\"><i class=\"icon-remove icon-white\"></i></button></form></center></td>";
						echo "</tr>";			
						}
					echo"</tbody>";	
					?>	
					</table>
				</div>
			</div>
		</div>
		<!-- /block -->
	</div>				
</div>

        <!--/.fluid-container-->

<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="vendors/jquery.uniform.min.js"></script>
<script src="vendors/chosen.jquery.min.js"></script>
<script src="vendors/bootstrap-datepicker.js"></script>


<script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
<script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

<script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="assets/form-validation.js"></script>
	
<script src="assets/scripts.js"></script>
<script>


$(function() {
	$(".datepicker").datepicker();
	$(".uniform_on").uniform();
	$(".chzn-select").chosen();
	$('.textarea').wysihtml5();
	

	$('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index+1;
		var $percent = ($current/$total) * 100;
		$('#rootwizard').find('.bar').css({width:$percent+'%'});
		// If it's the last tab then hide the last button and show the finish instead
		if($current >= $total) {
			$('#rootwizard').find('.pager .next').hide();
			$('#rootwizard').find('.pager .finish').show();
			$('#rootwizard').find('.pager .finish').removeClass('disabled');
		} else {
			$('#rootwizard').find('.pager .next').show();
			$('#rootwizard').find('.pager .finish').hide();
		}
	}});
	$('#rootwizard .finish').click(function() {

	var dataString = $('#form1').serialize() + '&' + $('#form2').serialize() + '&' + $('#form3').serialize();
	alert(dataString);
	
	$.ajax({
	type: "POST",
	url: "inserts/usuarios_insert.php",
	data: dataString,
	cache: false,
	success: function(html){
	alert('Usuário Cadastrado com Sucesso!', dataString);
	}
	});
	$('#rootwizard').find("a[href*='tab1']").trigger('click');
	$(':input','#form1')
	.not(':button, :submit, :reset, :hidden')
	.val('')
	.removeAttr('checked')
	.removeAttr('selected');
	$(':input','#form2')
	.not(':button, :submit, :reset, :hidden')
	.val('')
	.removeAttr('checked')
	.removeAttr('selected');
	
	return false;
	});
});
	</script>
</body>
</html>