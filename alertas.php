<?php include('header.php'); ?>
                <div class="span9" id="content">
                      <!-- morris stacked chart -->			
                <div class="row-fluid">
                        <!-- block -->
                    <div class="block" id="lista-alertas">
                        <div class="navbar navbar-inner block-header">
                            <div class="muted pull-left">Alertas Cadastrados</div>
                        </div>
                        <div class="block-content collapse in">
							<p><a href="#modal">Inserir Alerta</a></p>	
							<iframe src="views/alertas-graph.php" width="100%" frameborder="0" scrolling="yes" height="800px"></iframe>
                        </div>
                        <!-- /block -->
					</div>

                </div>
				
            </div>
            <hr>

        </div>
        <!--/.fluid-container-->
        <link href="js/datepicker.css" rel="stylesheet" media="screen">
        <link href="js/uniform.default.css" rel="stylesheet" media="screen">
        <link href="js/chosen.min.css" rel="stylesheet" media="screen">

        <link href="js/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="js/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.uniform.min.js"></script>
        <script src="js/chosen.jquery.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>

        <script src="js/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="js/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="js/wizard/jquery.bootstrap.wizard.min.js"></script>


        <script src="css/scripts.js"></script>
        <script>
        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>

	<div id="modal">
		<div class="modal-content">
			<div class="copy">
				<form class="form-horizontal" id="cadastroalerta" name="cadastroalerta" method="post" action="inserts/cadastro-alerta.php">
				  <fieldset>
					<legend>inserir dados alerta</legend>
					<div class="control-group">
					  <label class="control-label" for="appendedinput">data</label>
					  <div class="controls">
						<input type="date" class="input-xlarge datepicker" id="data" value="02/16/12">
					  </div>
					</div>							
					<div class="control-group">
						<label class="control-label" for="appendedinput">palavra chave</label>
						<div class="controls">
						  <div class="input-append">
								<select name="chave" id="selecterror3">
									<option>alagamento sp</option>
									<option>chuva sp</option>
									<option>enchente sp</option>
									<option>greve sp</option>
									<option>paralisação sp</option>
								</select>
						  </div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="appendedinput">categoria</label>
						<div class="controls">
						  <div class="input-append">
								<select name="categoria" id="selecterror3">
									<option>web</option>
									<option>noticias</option>
								</select>
						  </div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="appendedinput">link</label>
						<div class="controls">
						  <input name="link" class="input-xlarge focused" id="focusedinput" type="text">
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label" for="appendedinput">titulo</label>
						<div class="controls">
						  <div class="input-append">
							<input name="titulo" class="input-xlarge focused" id="focusedinput" type="text">
						  </div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="appendedinput">descrição</label>
						<div class="controls">
						  <div class="input-append">
							<textarea name="descricao" id="textarea2" rows="3"></textarea>
						  </div>
						</div>
					</div>	
					<button type="submit" class="btn btn-primary">inserir</button>
					<a href="#" class="btn">Fechar</a>
				  </fieldset>
				</form>   		
			<?php //Transferir o arquivo
			if (isset($_POST['submit'])) {
			  
				if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
					echo "<h1>" . "File ". $_FILES['filename']['name'] ." transferido com sucesso ." . "</h1>";
				}
			  
				//Importar o arquivo transferido para o banco de dados
				$handle = fopen($_FILES['filename']['tmp_name'], "r");
			  
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					$conexao = mysql_connect("localhost", "F0116002", "gmatos.039");
					$banco="mps";
					$bd=mysql_select_db($banco, $conexao);
					$import="INSERT into alertas(data,chave,link,categoria,descricao,titulo)values('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]')";
			  
					mysql_query($import) or die(mysql_error());
				}
			  
				fclose($handle);
			  
				print "Importação feita.";
			  
			//Visualizar formulário de transferência
			} else {
			  
				print "<form enctype='multipart/form-data' action='clima.php' method='post'>";
				print "<input size='50' type='file' name='filename'><br />\n";
				print "<input type='submit' name='submit' value='Upload'></form>";
			}
			?>							
			</div>		
		</div>
		<div class="overlay"></div>
	</div>
		
 </body>